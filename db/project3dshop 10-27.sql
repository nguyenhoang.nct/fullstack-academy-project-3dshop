-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3307
-- Thời gian đã tạo: Th10 27, 2020 lúc 08:15 AM
-- Phiên bản máy phục vụ: 10.4.13-MariaDB
-- Phiên bản PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `status`, `created_at`, `category`, `description`) VALUES
(1, 1, '2020-10-26 04:32:12', 'INTERIOR', 'INTERIOR'),
(2, 1, '2020-10-26 04:32:12', 'MODELS', 'MODELS'),
(3, 0, '2020-10-26 04:32:12', 'EXTERIOR', 'EXTERIOR');


DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_10_22_074242_create_products_table', 1),
(2, '2020_10_22_075855_create_categories_table', 2),
(3, '2020_10_26_043616_create_order_table', 3),
(4, '2020_10_26_044332_create__order_detail_table', 4),
(5, '2020_10_26_044712_create__users_table', 5),
(6, '2020_10_26_044332_create_order_detail_table', 6),
(7, '2020_10_26_044712_create_users_table', 6),
(8, '2020_10_26_044332_create_orderdetail_table', 7),
(9, '2020_10_26_061832_create_orderdetail_table', 8),
(10, '2020_10_26_062047_create_user_table', 9),
(11, '2020_10_26_062334_create_orderdetail_table', 10),
(12, '2020_10_26_062849_create_user_table', 10);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE IF NOT EXISTS `order_detail` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` tinyint(4) NOT NULL,
  `product_id` tinyint(4) NOT NULL,
  `amount` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `newprice` double(8,2) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `image1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `status`, `created_at`, `title`, `description`, `price`, `discount`, `newprice`, `amount`, `image1`, `image2`, `image3`, `image4`, `image5`, `category_id`) VALUES
(1, 1, '2020-10-26 04:06:30', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, NULL, NULL, NULL, NULL, NULL, 1),
(2, NULL, '2020-10-22 07:56:08', 'vol1', 'sdsds', 11.00, 11.00, 22.00, 33, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, '2020-10-22 07:56:08', 'vol2', 'sdsdsds', 11.00, 11.00, 33.00, 331, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, '2020-10-22 07:56:08', 'vol4', 'sdsdsds', 11.00, 11.00, 33.00, 331, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `age` tinyint(4) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favoutites` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
