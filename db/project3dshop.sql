-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2020 at 07:24 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project3dshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `url`, `created_at`, `updated_at`) VALUES
(1, 'http://localhost:3000/Banner/wallpapers222.png', NULL, NULL),
(2, 'http://localhost:3000/Banner/180521.jpg', NULL, NULL),
(3, 'http://localhost:3000/Banner/382953.jpg', NULL, NULL),
(4, 'http://localhost:3000/Banner/589406.jpg', NULL, NULL),
(5, 'http://localhost:3000/Banner/594203.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `status`, `created_at`, `category`, `description`) VALUES
(1, 1, '2020-10-26 04:32:12', 'INTERIOR', 'INTERIOR'),
(2, 1, '2020-10-26 04:32:12', 'MODELS', 'MODELS'),
(3, 0, '2020-10-26 04:32:12', 'EXTERIOR', 'EXTERIOR');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_10_22_074242_create_products_table', 1),
(2, '2020_10_22_075855_create_categories_table', 2),
(3, '2020_10_26_043616_create_order_table', 3),
(4, '2020_10_26_044332_create__order_detail_table', 4),
(5, '2020_10_26_044712_create__users_table', 5),
(6, '2020_10_26_044332_create_order_detail_table', 6),
(7, '2020_10_26_044712_create_users_table', 6),
(8, '2020_10_26_044332_create_orderdetail_table', 7),
(9, '2020_10_26_061832_create_orderdetail_table', 8),
(10, '2020_10_26_062047_create_user_table', 9),
(11, '2020_10_26_062334_create_orderdetail_table', 10),
(12, '2020_10_26_062849_create_user_table', 10),
(13, '2020_11_11_035832_create_table_banners_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` tinyint(4) NOT NULL,
  `product_id` tinyint(4) NOT NULL,
  `amount` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `newprice` double(8,2) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `image1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `status`, `created_at`, `title`, `description`, `price`, `discount`, `newprice`, `amount`, `image1`, `image2`, `image3`, `image4`, `image5`, `category_id`) VALUES
(1, 1, '2020-10-30 06:41:52', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, 'https://i.imgur.com/fHZVlsR.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1),
(2, NULL, '2020-10-30 06:41:59', 'vol1', 'sdsds', 11.00, 11.00, 22.00, 33, 'https://i.imgur.com/fHZVlsR.jpg', '../../../../images/Interior/product2.jpg', '../../../../images/Interior/product2.jpg', '../../../../images/Interior/product2.jpg', '../../../../images/Interior/product2.jpg', NULL),
(3, NULL, '2020-10-30 06:42:17', 'vol2', 'sdsdsds', 11.00, 11.00, 33.00, 331, 'https://i.imgur.com/Q5TwZLC.jpg', '../../../../images/Interior/product3.jpg', '../../../../images/Interior/product3.jpg', '../../../../images/Interior/product3.jpg', '../../../../images/Interior/product3.jpg', NULL),
(4, NULL, '2020-10-30 06:42:21', 'vol4', 'sdsdsds', 11.00, 11.00, 33.00, 331, 'https://i.imgur.com/Q5TwZLC.jpg', '../../../../images/Interior/product4.jpg', '../../../../images/Interior/product4.jpg', '../../../../images/Interior/product4.jpg', '../../../../images/Interior/product4.jpg', NULL),
(5, 1, '2020-10-30 02:49:23', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1),
(6, 1, '2020-10-30 02:49:23', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1),
(7, 1, '2020-10-30 02:49:23', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1),
(8, 1, '2020-10-30 02:49:23', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1),
(9, 1, '2020-10-30 02:49:23', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1),
(10, 1, '2020-10-30 02:49:23', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1),
(11, 1, '2020-10-30 02:49:23', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1),
(12, 1, '2020-10-30 02:49:23', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1),
(13, 1, '2020-10-30 02:49:23', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1),
(14, 1, '2020-10-30 02:49:23', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1),
(15, 1, '2020-10-30 02:49:23', '3d-INTERIOR Vol 1', '3DARCINTERIOR VOL 17 consists of 666 interior scenes with various designs and styles.\r\n   This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. \r\n   All presented renders are with postproduction.\r\n   Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.\r\nTotal size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).\r\n   3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.\r\nThis collection contains :\r\n\r\nI - PUBLIC WORKS\r\n\r\n01 - Hotel - Lobby & Reception : CLICK TO PAGE 01\r\n02 - Hotel - Restaurant : CLICK TO PAGE 02\r\n03 - Hotel - Restaurant VIP : CLICK TO PAGE 03\r\n04 - Coffee : CLICK TO PAGE 04\r\n05 - Hotel - Room : CLICK TO PAGE 05\r\n\r\nII - INTERIOR DESIGN ALLIANCE\r\n\r\n15 - Hotel : CLICK TO PAGE 15  \r\n16 - Restaurant : CLICK TO PAGE 16\r\n17 - Interior Design : CLICK TO PAGE 17\r\n… and lots of other interior scenes, you can see more images below or download attached catalogue.\r\n\r\nAvailable formats :\r\n 	-  V-Ray *.max - 1.5 or higher - with textures and shaders\r\n 	-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)', 150.00, 0.00, 150.00, 100, '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', '../../../../images/Interior/product1.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `age` tinyint(4) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skype` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `insta` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favoutites` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
