

export const ADD_TO_CART = "ADD_TO_CART";
//add cart action
export const addToCart= (newItem)=>{
    return{
        type: ADD_TO_CART,
        newItem
    }
}
export const REMOVE_ITEM = "REMOVE_ITEM";
//remove item action
export const removeItem=(id)=>{
    return{
        type: REMOVE_ITEM,
        id
    }
}
export const SUB_QUANTITY = "SUB_QUANTITY";
//subtract qt action
export const subtractQuantity=(id)=>{
    return{
        type: SUB_QUANTITY,
        id
    }
}
export const ADD_QUANTITY = "ADD_QUANTITY";
//add qt action
export const addQuantity=(id)=>{
    return{
        type: ADD_QUANTITY,
        id
    }
}
export const LOGIN_USER = "LOGIN_USER";
export const login=(statusLogin)=>{
    return{
        type: LOGIN_USER,
        statusLogin
    }
}
export const LOGOUT_USER = "LOGOUT_USER";
export const logout=()=>{
    return{
        type: LOGOUT_USER,
        
    }
}