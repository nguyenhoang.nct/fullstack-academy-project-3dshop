
// import { ADD_TO_CART,REMOVE_ITEM,SUB_QUANTITY,ADD_QUANTITY, LOGIN_USER, LOGOUT_USER } from '../action/action';
// import _find from "lodash/find";
// import _filter from "lodash/filter";

// const initState = {
//     items: [
//         {id: 1,img: "https://i.imgur.com/fHZVlsR.jpg" , rate: 4,name: "3d-INTERIOR Vol 1",price: 400,discount_price: 350.00,model:"ANI"},
//         {id: 2, img: "https://i.imgur.com/Q5TwZLC.jpg" , rate: 5,name: "3d-INTERIOR Vol 2",price: 450.00, discount_price: 390.00,model:"Ani"},
//         {id: 3, img: "https://i.imgur.com/BRA2ZJk.jpg" , rate: 5,name: "3d-INTERIOR Vol 4",price: 360.00,discount_price: 340.00,model:"Xus"},
//         {id:4, img: "https://imgur.com/9ygOPPE.jpg" , rate: 4, name: "3d-INTERIOR Vol 1",price: 599.00,discount_price: 520.00,model:"XUS"},
//         {id: 5, img: "https://imgur.com/WgWkTua.jpg" ,  rate: 5,name: "3d-INTERIOR Vol 3",price: 230.00, discount_price: 200.00,model:"ANI"},
//         {id:6, img: "https://imgur.com/7JqLPJ0.jpg" ,  rate: 4,name: "3d-INTERIOR Vol 1",price: 340.00,discount_price: 310.00,model:"AnI" },
//         {id: 7, img: "https://i.imgur.com/BRA2ZJk.jpg" ,  rate: 4, name: "3d-INTERIOR Vol 2",price: 250.00, discount_price: 210.00 ,model:"ANI"},
//         {id:8, img: "https://i.imgur.com/BRA2ZJk.jpg" ,  rate: 4,name: "3d-INTERIOR Vol 3",price: 320.00,discount_price: 299.00,model:"XUS"}
//     ],
//     addedItems:[],
//     total: 0, 
//     leng: 0
// }


//  const addCart = (state = initState, action) => {
//     switch (action.type) {
  
//       case ADD_TO_CART:
//         console.log("Add cart");
//         let addedItem = state.items.find(item=> item.id === action.id)
//         //check if the action id exists in the addedItems
//        let existed_item= state.addedItems.find(item=> action.id === item.id)
//        if(existed_item)
//        {
//           addedItem.quantity += 1 
          
//            return{
//               ...state,
//                total: state.total + addedItem.discount_price ,
//                 leng: state.leng + 1
//                 }
//       }
//         else{
//             addedItem.quantity = 1;
//             //calculating the total
//             let newTotal = state.total + addedItem.discount_price 
//             let count = addedItem.quantity
//             return{
//                 ...state,
//                 addedItems: [...state.addedItems, addedItem],
//                 total : newTotal,
//                 leng: state.leng + addedItem.quantity 
//             }
//         }
//         case REMOVE_ITEM:
//             let itemToRemove=  _find(state.addedItems, function(o) {return o.id === action.id});
//             let new_items = state.addedItems.filter(item=> action.id !== item.id);
//            //calculating the total
//     const newTotal = state.total - (itemToRemove.discount_price * itemToRemove.quantity)
//     return{
//         ...state,
//         addedItems:  new_items ,
//         total: newTotal,
//         leng: state.leng -  itemToRemove.quantity
//     }
//     case ADD_QUANTITY:
//     //INSIDE CART COMPONENT
//     let addedItem1 = state.items.find(item=> item.id === action.id)
//     addedItem1.quantity += 1 
//     let newTotal1 = state.total + addedItem1.discount_price

//     return{
//         ...state,
//         total: newTotal1,
//         leng: state.leng + 1
//     }

//     case SUB_QUANTITY:
//         //INSIDE CART COMPONENT
//         let addedItem2 = state.items.find(item=> item.id === action.id) 
//     //if the qt == 0 then it should be removed
//     if(addedItem2 && addedItem2.quantity === 1){
//         let new_items = state.addedItems.filter(item=>item.id !== action.id)
//         let newTotal = state.total - addedItem2.discount_price
//         return{
//             ...state,
//             addedItems: new_items,
//             total: newTotal,
//             leng: state.leng - 1
//         }
//     }
//     else {
//         addedItem2.quantity -= 1
//         let newTotal = state.total - addedItem2.discount_price
//         return{
//             ...state,
//             total: newTotal,
//             leng: state.leng - 1
//         }
// }
 

// case LOGIN_USER: 
// if(action.statusLogin == "success")
// {
//     return {
//         ...state,
//         login: true
//     }
    
// }
// else{
//     return{
//         ...state,
//         login: false
//     }
//  }
//  case LOGOUT_USER: 
//     return{
//         ...state,
//         login: false
//     }
      
//         default:
//             return state

//     }
// }

// export default addCart;




import { ADD_TO_CART,REMOVE_ITEM,SUB_QUANTITY,ADD_QUANTITY,LOGIN_USER, LOGOUT_USER } from '../action/action';
import _find from "lodash/find";
import _filter from "lodash/filter";

const initState = {
    items:[],
    addedItems:[],
    total: 0, 
    leng: 0,
    login: ""
}


 const addCart = (state = initState, action) => {
    switch (action.type) {

      case ADD_TO_CART:
        
        let addedItem = {  id: action.newItem.id, name: action.newItem.name, image: action.newItem.image,price: action.newItem.price,model: action.newItem.model,}
        state.items = [...state.addedItems, addedItem]
        let addItem = state.items.find(item=> item.id === action.newItem.id)
        //check if the action id exists in the addedItems
        console.log( addItem );
        let existed_item=  state.addedItems.find(item=> action.newItem.id === item.id)
       console.log(existed_item);
       if(existed_item)
       {
        addItem.quantity = addItem.quantity+1; 
           return{
              ...state,
               total: state.total + addItem.price ,
                leng: state.leng + 1
                }
      }
        else{
            addItem.quantity = 1;
            //calculating the total
            let newTotal = state.total + addItem.price 

            return{
                ...state,
                addedItems: [...state.addedItems, addItem],
                total : newTotal,
                leng: state.leng + addedItem.quantity 
            }
        }

        case REMOVE_ITEM:
            let itemToRemove=  _find(state.addedItems, function(o) {return o.id === action.id});
            let new_items = state.addedItems.filter(item=> action.id !== item.id);
           //calculating the total
    const newTotal = state.total - (itemToRemove.price * itemToRemove.quantity)
    return{
        ...state,
        addedItems:  new_items ,
        total: newTotal,
        leng: state.leng -  itemToRemove.quantity
    }

    case ADD_QUANTITY:
    //INSIDE CART COMPONENT
    let addedItem1 = _find(state.addedItems, function(o) {return o.id === action.id});
    console.log(addedItem1)
    // state.items.find(item=> item.id === action.id)
    addedItem1.quantity += 1 
    let newTotal1 = state.total + addedItem1.price

    return{
        ...state,
        total: newTotal1,
        leng: state.leng + 1
    }

    case SUB_QUANTITY:
        //INSIDE CART COMPONENT
        let addedItem2 = state.addedItems.find(item=> item.id === action.id) 
    //if the qt == 0 then it should be removed
    if(addedItem2.quantity === 1){
        let new_items = state.addedItems.filter(item=>item.id !== action.id)
        let newTotal = state.total - addedItem2.price
        return{
            ...state,
            addedItems: new_items,
            total: newTotal,
            leng: state.leng - 1
        }
    }
    else {
        addedItem2.quantity -= 1
        let newTotal = state.total - addedItem2.price
        return{
            ...state,
            total: newTotal,
            leng: state.leng - 1
        }
}
 
    case LOGIN_USER: 
    if(action.statusLogin == "success")
    {
        return {
            ...state,
            login: true
        }
        
    }
    else{
        return{
            ...state,
            login: false
        }
     }
     case LOGOUT_USER: 
        return{
            ...state,
            login: false
        }
      
        default:
            return state

    }
  

}

export default addCart;


