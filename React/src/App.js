import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import Header from './Components/Header';
import Footer from './Components/Footer';
import styled from 'styled-components';
import BackGRD from './images/BG/pexels-photo.png';
import Home from './Pages/HomePage';
import Exterior from './Pages/ExteriorPage';
import Interior from './Pages/InteriorPage';
import FAQ from './Pages/FAQPage';
import Cart from './Pages/CheckoutCartPage';
import Contact from "./Pages/ContactPage";
import {Switch,Route} from "react-router-dom";
import { BrowserRouter} from "react-router-dom";
import ProductDetail from "./Pages/ProductDetailPage";
import Profile from "./Pages/ProfilePage";
const Wrapper = styled.div`
background-size: auto;
`;

function App() {
  return (
   <Wrapper style={{backgroundImage: `url(${BackGRD})`}}>
      <BrowserRouter>
  <Header />
  <Switch>
          <Route exact path="/" component={Home} />
          <Route  path="/Interior" component={Interior} />  
          <Route  path="/Profile" component={Profile} />  
          <Route  path="/Exterior" component={Exterior} />  
          <Route  path="/Faq" component={FAQ} /> 
          <Route  path="/Contact" component={Contact} /> 
          <Route  path="/Cart" component={Cart} /> 
          <Route path="/detail/:id/" component={ProductDetail} />
        </Switch>

  <Footer />
  </BrowserRouter>
   </Wrapper>
  );
}

export default App;
