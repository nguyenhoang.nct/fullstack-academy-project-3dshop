import React from 'react';
import {Container,Row, Col} from 'reactstrap'
import {GALLERY} from '../const/common';
import styled from 'styled-components';

const Title = styled.div`
    background-color: #082157;
    display: flex;
    position: relative;
    margin-top: 30px;
    padding: .5rem 1rem;
    h2{
        padding-top: 5.5px;
        margin-bottom: 2.5px;
        top: 14px;
        left:20px;
        color: #ffffff;
        font-size: 18px;
        font-family: iCielNovecentosansDemiBold;
        text-transform: uppercase;
    }

`;

function Gallery() {
    const data = GALLERY.map((item) => {
    return (
       
        <Col md="3" style={{paddingLeft: "0px",paddingTop: "10px",paddingRight: "10px"}} >
            <img src={item.img} style={{width: "60px",height: "60px",objectFit: "cover",cursor:"pointer"}} />
        </Col>
       
      );
    });
    return (
        <>
            <Title>
                <h2>gallery</h2>

            </Title>
        
            <Container>
                <Row style={{marginRight: "-19px"}}>
                {data}
            </Row>
            </Container>
        </>
    );
}

export default Gallery;