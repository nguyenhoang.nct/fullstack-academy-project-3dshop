import React from 'react';

import {Container,Row, Col} from 'reactstrap';
import Adv1 from "../../images/Ads/Adv1.jpg";
import Adv2 from "../../images/Ads/Adv2.jpg";
import Ob from "../../images/BG/Object.png";
import Logo from "../../images/BG/logo.png";
import styled from 'styled-components';
import LogoText from '../../images/BG/LogoText.png';
import Visa from '../../images/Pay/visa.png';
import Maestro from '../../images/Pay/maestro.png';
import Paypal from '../../images/Pay/paypal.png';
import Mastercard from '../../images/Pay/mastercard.png';
import Discover from '../../images/Pay/discover.png';
const Row1 = styled(Row)`
background: #ffffff;
padding-top: 20px;
padding-bottom: 20px;
`;
const ColCus = styled(Col)`
    p{
        padding-top: 20px;
        font-size: 14px;
        font-family: segoe_ui;
        color: #ffffff;
        span{
            color: #f49101;
        }
    }
`;
const Col2 = styled(Col)`
    ul{
        display: flex;
        list-style: none;
        margin-bottom: 0;
        li{
            padding-left: 15px;
            padding-right: 15px;
            position: relative;
            &:first-of-type{
                padding-left: 20px;
            }
            &::after{
                position: absolute;
                content: "";
                width: 2px;
                height: 16px;
                background: green;
                top: 5px;
                right: 0;
            }
            &:last-of-type{
                &::after{
                    content: none;  
                }
            }
            & a{
                color: #ffffff;
                text-decoration: none;
                text-transform: uppercase;
                font-size: 18px;
                font-family: iCielNovecentosansNormal;
            }
        }
    }
`;
const Pay = styled.div`
padding-top: 23px;
display: flex;
margin-left: 40px;
    img{
        padding-right: 16px;
    }
`;
function Footer() {
    return (
     <Container style={{marginTop: "20px"}}>
<Row1>
    <Col md="4" style={{paddingRight: "0"}}>
    <a href="https://www.youtube.com/watch?v=Gld1iKNQCM0&list=PLA422A43808F3A00A&index=2">
    <img src={Adv1}  className="w-100" style={{cursor: "pointer"}}/> 
   </a>
    </Col>
    <Col md="4" style={{paddingRight: "0"}}>
    <a href="https://arkhamdb.com/deck/view/375313">
    <img src={Adv2} className="w-100" style={{cursor: "pointer"}}/>
   </a>
    </Col>
    <Col md="4" style={{display: "flex"}}>
    <img src={Ob} style={{objectFit: "contain"}} /> 
<img src={Logo} style={{objectFit: "contain"}}/>
    </Col>

</Row1>
<Row style={{marginTop: "30px"}}>
<ColCus md="3">
<img src={LogoText} />
<p>Copyright 2014 by <span>baobinh.net</span></p>
</ColCus>
<Col2 md="9">
        <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/contact">CONTACT</a></li>
            <li><a href="/blog">BLOG</a></li>
            <li><a href="/about">about us</a></li>
        </ul>
        <Pay>
        <a href="/">
        <img src = {Visa} />
        </a>
        <a href="/">
        <img src = {Maestro} />
        </a>
        <a href="/">
        <img src = {Paypal} />
        </a>
        <a href="/">
        <img src = {Mastercard} />
        </a>
        <a href="/">
        <img src = {Discover} />
        </a>
        </Pay>
</Col2>
</Row>
     </Container>
    );
}

export default Footer;