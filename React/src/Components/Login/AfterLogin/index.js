import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import { Button, Col, Container, Row } from 'reactstrap';
import user from "../../../images/User/user1.png";
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
// import {logout} from "../../../redux/action/action";
const StyleContainer = styled(Container)`
    border: 2px solid #cccccc;
    padding-bottom: 5px;
`;

const StyleRow = styled(Row)`
text-align: -webkit-center;
display: block !important;
padding-top: 50px;
    img{
        
        object-fit: cover;
        width: 100px;
        height: 100px;
        border: 2px solid #999999;
    }
    h3{
        padding-top: 10px;
        text-transform: uppercase;
        color: #000000;
        font-size: 18px;
        font-family: segoe_ui; 
        font-weight: bold;
        margin-bottom: 3px;
    }
    p{
        color: #333333;
        font-size: 12px;
        font-family: segoe_ui; 
    }
`;
const StyleButton = styled(Button)`
color: #ffffff;
font-size: 12px !important;;
font-family: iCielNovecentosansNormal;
background-color: #333333 !important;
padding: 10px 7px 11px 6px !important;
width: 100%;
 border: 1px solid #cccccc !important;
border-radius: 0 !important;
margin-bottom: 10px;
    &:hover{
        background-color: #f49101 !important;
    }
    
`;
function AfterLogin() {
const handleLogOut = () =>{
    localStorage.removeItem('x-access-token');
  }
    return(
       <StyleContainer>
    <StyleRow>
      <img src={user} /> 
      <h3>weyne owen</h3>
      <p>Architectural designer</p>
    </StyleRow>
    <Row>
        <Col md="6">
        <Link to="/cart"> <StyleButton>MANAGE CART</StyleButton> </Link> 
        </Col>
        <Col md="6">
        <StyleButton>WISHLIST</StyleButton>
        </Col>
        <Col md="6">
        <StyleButton>UPDATE</StyleButton>
        </Col>
        <Col md="6">
        <StyleButton onClick={handleLogOut} href="/">LOG OUT</StyleButton>
        </Col>
    </Row>
       </StyleContainer>
    )
}
const mapStateToProps = state => ({
    login: state.login
  })
//   const mapDispatchToProps = dispatch => ({
  
//     logout: ()=>{dispatch(logout())}
//   })
  export default connect(mapStateToProps, null)(AfterLogin);
