import Axios from 'axios';
import React, { useState,useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import styled from 'styled-components';
import {
    useLocation, useHistory 
  } from "react-router-dom";
import {login} from "../../../Respository/respository";
import { connect } from "react-redux";
const Title = styled.div`
    background-color: #082157;
    display: flex;
    position: relative;
    padding: .5rem 0.8rem;
    h2{
        padding-top: 5.5px;
        margin-bottom: 2.5px;
        top: 14px;
        left:20px;
        color: #ffffff;
        font-size: 18px;
        font-family: iCielNovecentosansDemiBold;
    }
    a{
        position: absolute;
        top: 20px;
        right:20px;
        color: #557c83;
        font-size: 10px;
        font-family: iCielNovecentosansNormal; 
        text-decoration: underline; 
        &:hover{
            color: #ffffff;
        }
    }
`;
const CustomForm = styled(Form)`
border: 1px solid #cccccc;
padding-left: 20px;
padding-right: 22px;
padding-bottom: 25px;
    
    p{
        color: #5b5150;
        font-size: 14px;
        font-family: segoe_ui;
        margin-bottom: 10px;
    }
    button{
        background: #333333;
        border-radius: 0;
        border: 2px solid #cccccc;
        color: #ffffff;
        font-size: 14px;
        font-family: iCielNovecentosansNormal; 
        padding: 8px 15px;
        &:hover{
            background-color: #333333;
            border-color: #cccccc;
        }
    }
    a{
        margin-top: 10px;
        color: #dd2c00;
        font-size: 14px;
        font-family: segoe_ui;
       
    }
`;


function BeforeLogin({path}) {
    const [name,setName] = useState("");
    const [pass,setPass] = useState("");
    const [value,setValue] = useState();
    let location = useLocation();
    let history = useHistory();

    const handleSubmit  = (e) => {
        e.preventDefault();
        const Users = {
          name: name,
            password: pass
        }
        // Axios.post('/signin', Users)
        //   .then(function (response) {
        //     console.log(response.data);
        //     // setLoad(response.data);
        //     login(response.data);
        //   })
        //   .catch(function (error) {
        //     console.log(error);
        //   });
        login(Users)
        .then(token => history.push(path))
        .catch(err => alert(err));
    }

    console.log(name);
    function validateForm() {
        return name.length > 0 && pass.length > 0;
      }
    return (
    <>
    <Title>
        <h2>JOIN US</h2>
        <a href="/">CREATE AN ACCOUNT</a>
    </Title>
    <CustomForm onSubmit = {handleSubmit}>
    <FormGroup style={{marginBottom: "0px "}}>
        <p style={{paddingTop: "30px"}}>Username</p>
        <Input type="text"  id="Username"  value={name} onChange={e => setName(e.target.value)} />
      </FormGroup>
      <FormGroup style={{marginBottom: "10px "}}>
        <p style={{marginTop: "5px"}}>Password</p>
        <Input type="password"  id="Password"  value={pass} onChange={e => setPass(e.target.value)}  />
      </FormGroup>
      <Button disabled={!validateForm()} type="submit">LOGIN</Button>
      <FormGroup check style={{paddingTop: "10px"}}>
        <Input type="checkbox"  id="exampleCheck"/>
        <Label for="Remember" check>Remember</Label>
      </FormGroup>
      <a href="/">Forgot your password?</a>
    </CustomForm>
    </>
    );
}
const mapStateToProps = state => ({
  data: state
})
// const mapDispatchToProps = dispatch => ({
//   login: (statusLogin) => dispatch(login(statusLogin))
// })
export default connect(mapStateToProps, null)(BeforeLogin);
