import Axios from 'axios';
import React, { useState,useEffect } from 'react';
import BeforeLogin from "./BeforeLogin";
import AfterLogin from "./AfterLogin";
import { connect } from "react-redux";
import { isAuthenticated } from '../../Respository/respository';
import {
  useLocation, useHistory 
} from "react-router-dom";
function Login() {
     let location = useLocation();
    let history = useHistory();
    return (
    <>
          {
               ( isAuthenticated() ) ? 
                (   <AfterLogin />) : 
                (  <BeforeLogin path ={location.pathname} /> )
             }
    </>
    );
}
const mapStateToProps = state => ({
  data: state
})
// const mapDispatchToProps = dispatch => ({
//   login: (statusLogin) => dispatch(login(statusLogin))
// })
export default connect(mapStateToProps, null)(Login);
