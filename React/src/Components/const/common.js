import Pic1 from '../../images/Gallery/1.jpg';
import Pic2 from '../../images/Gallery/2.jpg';
import Pic3 from '../../images/Gallery/3.jpg';
import Pic4 from '../../images/Gallery/4.jpg';
import Pic5 from '../../images/Gallery/5.jpg';
import Pic6 from '../../images/Gallery/6.jpg';
import Pic7 from '../../images/Gallery/7.jpg';
import Pic8 from '../../images/Gallery/8.jpg';
import Pic9 from '../../images/Gallery/9.jpg';
import Pic10 from '../../images/Gallery/10.jpg';
import Pic11 from '../../images/Gallery/11.jpg';
import Pic12 from '../../images/Gallery/12.jpg';


export const GALLERY = [
    {
        img: Pic1 
    },
    {
        img: Pic2 
    },
    {
        img: Pic3
       
    },
    {
        img: Pic4 
       
    },
    {
        img: Pic5 
    },
    {
        img: Pic6 
    },
    {
        img: Pic7 
    },
    {
        img: Pic8 
    },
    {
        img: Pic9 
    },
    {
        img: Pic10 
    },
    {
        img: Pic11
    },
    {
        img: Pic12 
    }
]

export const BLOG = [
    {
       title: "5 VAARFINA SEATING",
       date: "2016-04-03 01:00:57."
    },
    {
        title: "Charming 1950-century villa with large patio and conservatory",
        date: "2016-04-03 00:49:34."
    },
    {
        title: "Newly opened restaurant Kadeau in Copenhagen",
        date: "2016-04-03 00:36:58."
       
    },
    {
        title: "Beautiful apartment turn of the century with period features",
        date: "2016-04-03 00:27:51."
       
    },
    {
        title: "Inspiration to patio from Sika design",
       date: "2016-04-03 00:18:00."
    },
    {
        title: "Louis Weisdorf Lamp and multi-lite",
       date: "2016-04-03 00:10:12."
    }
]


export const FAQ = [
    {
        question: "DO I NEED TO REGISTER AN ACCOUNT TO BUY MODELS?",
       answer: `Yes. A free account registration is required in order to purchase and download any 3d models from 3DArcShop catalogue.`,
       tag: "Sign-up now",
       index: 0
    },
    {
        question: " I CAN'T LOG IN. PASSWORD RESET IS NOT WORKING. WHAT TO DO?",
        answer: `Yes. A free account registration is required in order to purchase and download any 3d models from 3DArcShop catalogue. `,
        tag: "Sign-up now",
        index: 1

    },
    {
        question: "I SENT YOU AN EMAIL AND HAVE NOT RECEIVED A RESPONSE.",
        answer: `Yes. A free account registration is required in order to purchase and download any 3d models from 3DArcShop catalogue. `,
        tag: "Sign-up now",
        index: 2
       
    },
    {
        question: "HOW CAN I PAY FOR MY ORDER?",
       answer: `Yes. A free account registration is required in order to purchase and download any 3d models from 3DArcShop catalogue. `,
       tag: "",
       index: 3
       
    },
    {
        question: "I PAID FOR THE PRODUCT. WHERE CAN I DOWNLOAD IT?",
        answer: `Yes. A free account registration is required in order to purchase and download any 3d models from 3DArcShop catalogue. `,
        tag: "",
        index: 4
    },
    {
        question: "I HAVE PAID FOR THE ORDER. I DON'T GET THE DOWNLOAD LINKS.",
        answer: `Yes. A free account registration is required in order to purchase and download any 3d models from 3DArcShop catalogue. `,
        tag: "",
        index: 5
    },
    {
        question: "WHY ARE 3DARCSHOP LIBRARIES ELECTRONICALLY DISTRIBUTED ONLY?",
        answer: `Yes. A free account registration is required in order to purchase and download any 3d models from 3DArcShop catalogue. `,
        tag: "Sign-up now",
        index: 6
    },
    {
        question: "I'M GETTING ERRORS DURING UNPACKING PRODUCT ARCHIVES.",
        answer: `Yes. A free account registration is required in order to purchase and download any 3d models from 3DArcShop catalogue. `,
        tag: "Sign-up now",
        index: 7
    },
    {
        question: "CONTACT",
        answer: `Yes. A free account registration is required in order to purchase and download any 3d models from 3DArcShop catalogue. `,
        tag: "Sign-up now",
        index: 8
    }
   

]