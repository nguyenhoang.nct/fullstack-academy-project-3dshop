import React from 'react';
import {BLOG} from '../const/common';
import styled from 'styled-components';
import Time from 'react-ionicons/lib/IosTimeOutline';

const Title = styled.div`
    background-color: #082157;
    display: flex;
    position: relative;
    margin-top: 30px;
    padding: .5rem 1rem;
    h2{
        padding-top: 5.5px;
        margin-bottom: 2.5px;
        top: 14px;
        left:20px;
        color: #ffffff;
        font-size: 18px;
        font-family: iCielNovecentosansDemiBold;
        text-transform: uppercase;
    }

`;
const Wrapper = styled.div`
    padding-top: 30px;
    padding-left: 18px;
    padding-right: 8px;
    border: 1px solid #cccccc;
`;
const Content = styled.div`
padding-bottom: 25px;
    a{
        color: #333333;
        font-size: 14px;
        font-family: segoe_ui;
       
       &:hover{
        text-decoration: none;
        color: #333333;
       }
    }
    span{
        color: #999999;
        font-size: 12px;
        font-family: segoe_ui;
        margin-left: 10px;
    }
`;
function Blog() {
    const data = BLOG.map((item) => {
    return (
      <Content>
          <a href ="/">{item.title}</a> <br />
                <Time  fontSize="12px" color="#999999" />
         <span>{ item.date}</span>  <br />
       </Content>     
       
      );
    });
    return (
    <>
    <Title>
        <h2>LATEST BLOG</h2>
    </Title>
   <Wrapper>
   {data}
        </Wrapper>
    </>
    );
}

export default Blog;