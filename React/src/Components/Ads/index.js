import React from 'react';
import ReactDOM from 'react-dom';
import {Container,Row, Col} from 'reactstrap'
import Buy from "../../images/Gallery/Buy.jpg"
import styled from 'styled-components';
import Ad from '../../images/Gallery/ADV.jpg'
const Title = styled.div`
    background-color: #082157;
    display: flex;
    position: relative;
    margin-top: 30px;
    padding: .5rem 1rem;

    h2{
        padding-top: 5.5px;
        margin-bottom: 2.5px;
        top: 14px;
        left:20px;
        color: #ffffff;
        font-size: 18px;
        font-family: iCielNovecentosansUltraBold;
        text-transform: uppercase;
        span{
            padding-left: 12px;
            font-family: iCielNovecentosansDemiBold;
            font-size: 14px;
            text-transform: uppercase;
            span{
                padding-left: 0;
                font-family: iCielNovecentosansNormal;
                font-size: 14px; 
                text-transform: none;
            }
        }
    }

`;

function Ads() {
  
    return (
    <>
    <Title>
        <h2>Ads <span>270<span>x</span>226px</span></h2>
    </Title>
    <a href="https://www.youtube.com/watch?v=CgWzaS170yk">
   <img src ={Buy} className="w-100" style={{marginTop: "10px"}} />
   </a>
   <Title  >
        <h2>Ads <span>270<span>x</span>226px</span></h2>
    </Title>

    <img src ={Ad} className="w-100" style={{marginTop: "10px",paddingBottom: "100px"}} />
    </>
    );
}

export default Ads;