import React ,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {Container,Row, Col, Button} from 'reactstrap';
import styled from 'styled-components';
import axios from 'axios';
import search from '../../../../image/Icon/search.png';
import { connect } from "react-redux";
import { increase } from '../../redux/action/action';

const CustomRow = styled(Row)`
  .col-md-4{
    &:first-of-type{
      padding-left: 0;
    }
    &:last-of-type{
      padding-right: 0;
    }
  }
`;

const CustomCol = styled(Col)`
cursor: pointer;
  p{
    font-size: 18px;
    color: #5b5150;
    font-family: iCielNovecentosansNormal;
    text-transform: uppercase;
    margin-bottom: 0;
  }
  img{
    width: 100%;
    height: 151px;
    object-fit: cover;
    margin-bottom: 10px;
  }
  span{
    margin-top: 20px;
    font-size: 18px;
    color: #f49101;
    font-family: iCielNovecentosansNormal; 
  }
  button{
    background: #cccccc;
    text-transform: uppercase;
    color: #333333;
    font-size: 14px;
    font-family: iCielNovecentosansNormal;
    border-radius: 0;
    margin-top: 10px;
  }
`;

function Interior({ items,
  addToCart}) {

  const [product,setProduct] = useState( {product: []} );
  // const [state, dispatch] = useReducer(myReducer, { count: 0 })
  
  useEffect(() => {
      const fetchData = async () => {
          const result = await axios(`/api/product` ,
          );
         
          setProduct(result.data);
         
        };
        fetchData();
      
    }, []);
     
    const handleClick = (id) => {
      addToCart(id);
  
   }

    const dataProduct = items.map((item) => {
      return (
        <CustomCol md = "4" >
      <p key={item.id}>{ item.name } </p> 
       <div className="product" style={{backgroundImage: `url(${item.img})`,height: "151px",backgroundPosition: "center",
  backgroundSize: "cover"}}>
       <img className="search"  src={search} style={{height: "auto",width: "auto",position: "absolute",top: "calc(50% - 17px)",
  left: "calc(50% - 16px)"}} /> 
       </div>
          <span>${ item.price }</span>
        
  <Button block  onClick={()=>{handleClick(item.id)}}>Add to Cart</Button>
  
        </CustomCol>

      );
      });
  return (
<>
<Container style={{background: "#ffffff",paddingTop: "50px"}}>
  <CustomRow>
  {dataProduct}  

  </CustomRow>
</Container>
<Pag />
</>
  );
}


const mapStateToProps = state => ({
 
  items: state.items
})
const mapDispatchToProps = dispatch => ({
  
  addToCart: (id)=>{dispatch(addToCart(id))}
})
export default connect(mapStateToProps, mapDispatchToProps)(Interior);
