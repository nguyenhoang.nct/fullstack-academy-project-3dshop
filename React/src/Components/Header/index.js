import React from 'react';
import ReactDOM from 'react-dom';
import {Container,Row, Col} from 'reactstrap';
import Ob from "../../images/BG/Object.png";
import BG from "../../images/BG/pexels-photo2.png";
import Logo from "../../images/BG/logo.png";
import styled from 'styled-components';
import Menu from '../Menu';
const Row1 = styled(Row)`
    margin-right: 0 ;
    margin-left: -9px;
   img{
       position: relative;
    &:nth-of-type(1){
        position: absolute;
        top: 36px;
        left: 57px;
    }
    &:nth-of-type(2){
        position: absolute;
        top: 53px;
        left: 178px;
    }
   }
   h1{
    position: absolute;
    font-size: 60px;
    text-transform: uppercase;
   }
`;

function Header() {
    return (
     <Container style={{paddingRight: "0",paddingLeft: "0"}}>
<Row1 style={{backgroundImage: `url(${BG})`,height: '150px', position: 'relative',marginRight: "0",marginLeft: "-10px"}}>
 <img src={Ob} /> 
<img src={Logo} />
</Row1>
<Menu />
     </Container>
    );
}

export default Header;