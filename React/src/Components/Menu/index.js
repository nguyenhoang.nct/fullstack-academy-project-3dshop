
import React, { useState, useEffect } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container, Row, Col, Input
} from 'reactstrap';
import {
  NavLink, Link
} from "react-router-dom";
import styled from "styled-components";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import Twitter from '../../images/Icon/twitter.png';
import Face from '../../images/Icon/facebook.png';
import Google from '../../images/Icon/g+.png';
import { connect } from "react-redux";

const CustomNavBar = styled(Navbar)`
background: #f49101;

margin: 0;
`;
const CusDropdownToggle = styled(DropdownToggle)`
&.nav-link{
    padding-top: 26px;
    padding-bottom: 23px;
    font-size: 16px;
    color: #ffffff !important;
    font-family: iCielNovecentosansMedium;
    &:hover{
        background: #ffffff ; 
        color: #082157 !important;
        }
}
`;
const CusDropMenu = styled(DropdownMenu)`
  border: 1px solid #f49101;
  z-index: 10;
  top: 63px;
  left: -58px;
  border-radius: 0;

  margin: 0;
  box-shadow: 0px 10px 10px 0px rgba(73,73,73,1);
  .dropdown-item{
    padding-top: 20px ;
    padding-bottom: 11px ;
    padding-left: 45px ; 
    padding-right: 56px ;
    font-size: 14px;
    background: #ffffff;
    color: #082157 ;
    font-family: iCielNovecentosansMedium;
    position: relative;
    &:focus{
      outline: none;
    }
    &:active{
      outline: none;
    }
    &:hover{
      color:#f49101;
   
    }
    &::after{
      position: absolute;
      content: "";
      width: 80%;
      height: 1px;
      background: #e6e6e6;
      bottom: 0;
      left: 14px;
    }
  }
`;
const CustomNavLink = styled(NavLink)` 
        padding-top: 26px !important;
        padding-bottom: 23px  !important;
        font-size: 16px;
        color: #ffffff !important;
        font-family: iCielNovecentosansMedium;
        &:hover{
            background: #ffffff; 
            color: #082157 !important;
            text-decoration: none;
            }  
       
`;
const StyleInput = styled(Input)`
width: 70%;
border-radius: 0;
border: 1px solid #cccccc;
margin-top: 30px;
padding-left:30px !important;

&:focus{
  box-shadow: none;
  border-color: #cccccc;
}
`;
const StyleA = styled.a`
font-size: 14px;
color:#cccccc;
position: absolute;
top: 37px;
left: 25px;
&:hover{
    color:#111111;
}
`;
const Col3 = styled(Col)`
  p{
    text-align: right;
    color: #666666;
    font-size: 14px;
    font-family: segoe_ui;
    padding-top: 40px;
    span{
      color: #dd2c00;
    }
  }
`;
const CusNavItem = styled(NavItem)`
display: contents;
`;
const Menu = ({items ,leng}) => {
  const [data,setData] = useState();
  const [isOpen, setIsOpen] = useState(false);
  const [hover , setHover] = useState(true)
  const toggle = () => setIsOpen(!isOpen);
  const [isHovered, setIsHovered] = useState(false);
  useEffect(() => {
    setData(items);
}, [items]);


  return (
    <>
      <CustomNavBar light expand="md">
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <CusNavItem>
              <CustomNavLink className="navlink1"  exact to="/">Home</CustomNavLink>
            </CusNavItem>
            <UncontrolledDropdown nav inNavbar 
              onMouseEnter={e => setIsHovered(true)}
              onMouseLeave={e => setIsHovered(false)}
              isOpen={isHovered}
            >
              <CusDropdownToggle nav 
              >
              Models
              </CusDropdownToggle>
              {
                isHovered &&
                <CusDropMenu center>
                  <DropdownItem>
                  character 1
                  </DropdownItem>
                  <DropdownItem>
                  character 2
                  </DropdownItem>
                  <DropdownItem>
                  character 3
                  </DropdownItem>
                  <DropdownItem>
                  character 4
                  </DropdownItem>
                </CusDropMenu>
              }
            </UncontrolledDropdown>
            <CusNavItem>
              <CustomNavLink className="navlink2"  exact to="/exterior"> exterior </CustomNavLink>
            </CusNavItem>
            <CusNavItem>
              <CustomNavLink className="navlink3" exact to="/interior">interior</CustomNavLink>
            </CusNavItem>
            <CusNavItem>
              <CustomNavLink className="navlink4" exact to="/Profile"> Free </CustomNavLink>
            </CusNavItem>
            <CusNavItem>
              <CustomNavLink className="navlink5"  exact to="/blog"> blog </CustomNavLink>
            </CusNavItem>
            <CusNavItem>
              <CustomNavLink className="navlink6"  exact to="/faq"> faq </CustomNavLink>
            </CusNavItem>
            <CusNavItem>
              <CustomNavLink className="navlink7"  exact to="/contact"> contact </CustomNavLink>
            </CusNavItem>
          </Nav>
       
        </Collapse>
      
      </CustomNavBar>
      <Container style={{background: "#ffffff"}}>
          <Row>
            <Col md="4" style={{paddingBottom: "30px"}}>
      <StyleInput  />
      <StyleA href = "#"><FontAwesomeIcon icon = {faSearch} /> </StyleA>
            </Col>
            <Col md="4" style={{position: "relative"}}>
              <img src ={Twitter} style={{position: "absolute",top:"35px",left: "115px",cursor: "pointer"}} />
              <img src ={Face} style={{position: "absolute",top:"33px",left: "180px",cursor: "pointer"}}/>
              <img src ={Google} style={{position: "absolute",top:"34px",left: "230px",cursor: "pointer"}} />
            </Col>
            <Col3 md="4">
              
              <p>Now in your cart 
              <Link to="/cart"><span> {leng} items</span></Link>  </p>
 
            </Col3>
          </Row>
        </Container>
    </>
  );
}


const mapStateToProps = state => ({
  items: state.addedItems,
  leng: state.leng,

})


export default connect(mapStateToProps, null)(Menu);


