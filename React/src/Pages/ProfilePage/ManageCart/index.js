import React, { useState } from 'react';
import { Col, Container, Row,Button, Input } from 'reactstrap';
import styled from 'styled-components';
import { Table } from 'reactstrap';

const CustomInput = styled(Input)`
    margin-top: 30px;
    height: calc(1.5em + .75rem + 4px) !important;
    font-size: 16px !important;
    color: #3b3b3b !important;
    width: 20% !important;
`;

const CustomTable = styled(Table)`
    margin-top: 30px;
  
   thead{
    tr{
        th{
            font-weight: bold;
            color: #3b3b3b;
            font-size: 16px;
            font-family: segoe_ui;
            text-align: center;
        }
    }
   }
   tbody{
       tr{
           td{
            color: #3b3b3b;
            font-size: 14px;
            font-family: segoe_ui;
            text-align: center;
            vertical-align: middle;
            a{
                margin-bottom: 5px;
                color: #333333;
                font-size: 14px;
                font-family: segoe_ui;
                text-decoration: underline;
                &:hover{
                    color: #f49101;
                }
            }
           }
           th{
            color: #3b3b3b;
            font-size: 14px;
            font-family: segoe_ui;
            text-align: center;
            vertical-align: middle;
           }
       }
   }
`;

const ManageCart = (props) => {

  return (
    <div>
      <Container>
      <CustomInput type="select" name="select" id="exampleSelect">
            <option>Recent Oders</option>
            <option>All Oders</option>
            <option>Completed Oders</option>
            </CustomInput>
      <CustomTable bordered>
      <thead>
        <tr>
          <th>Order #</th>
          <th>Date</th>
          <th>Ship to</th>
          <th>Order Total</th>
          <th>Status</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">001</th>
          <td>09/09/10</td>
          <td>Billing address 1</td>
          <td>$500.00</td>
          <td>Pending</td>
          <td style ={{display:"grid"}}>  <a href="/">View Order</a> 
          <a href="/">Reorder</a> </td>
        </tr>
        <tr>
          <th scope="row">002</th>
          <td>09/09/10</td>
          <td>Billing address 2</td>
          <td>$500.00</td>
          <td>Pending</td>
          <td style ={{display:"grid"}}>  
          <a href="/">View Order</a> 
          <a href="/">Reorder</a> </td>
        </tr>
      
      </tbody>
    </CustomTable>
      </Container>
    </div>
  );
}

export default ManageCart;