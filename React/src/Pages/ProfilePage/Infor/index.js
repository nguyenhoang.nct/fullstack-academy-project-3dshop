import React, { useState } from 'react';
import { Col, Container, Row,Button } from 'reactstrap';
import styled from 'styled-components';
import user from "../../../images/User/user1.png"
import Pin from 'react-ionicons/lib/IosPin';
import LogoSkype from 'react-ionicons/lib/LogoSkype';
import Phone from 'react-ionicons/lib/IosPhonePortrait';
import Mail from 'react-ionicons/lib/IosMail';
import LogoTwitter from 'react-ionicons/lib/LogoTwitter';
import LogoLinkedin from 'react-ionicons/lib/LogoLinkedin';

import { Icon, InlineIcon } from '@iconify/react';
import socialGoogleplus from '@iconify-icons/ion/social-googleplus';
import socialFacebook from '@iconify-icons/ion/social-facebook';

const Row1 = styled(Row)`
margin-top: 30px;
  .col-md-4{
    text-align: center;
    img{
      width: auto;
      height: auto;
 
    }
    h2{
      padding-top: 10px;
      font-size: 18px;
      color: #000000;
      font-family: segoe_ui;
      font-weight: bold;
      text-transform: uppercase;
      margin-bottom: 5px;
    }
    p{
      font-size: 12px;
      color: #333333;
      font-family: segoe_ui;

    }
  }
`;
const Row2 = styled(Row)`
margin-top: 30px;
.col-md-8{
    h4{
      font-size: 14px;
      color: #000000;
      font-family: segoe_ui;
      font-weight: bold;
    }
    ul{
      list-style: none;
      padding-left: 0;
      li{
        padding-top: 30px;
         span{
          font-size: 14px;
          color: #666666;
          font-family: segoe_ui;
         } 
      }
    }
}
.col-md-4{
  text-align: left;
  h4{
    font-size: 14px;
    color: #000000;
    font-family: segoe_ui;
    font-weight: bold;
  }
  p{
    font-size: 14px;
    color: #666666;
    font-family: segoe_ui;
  }
}
`;

const Content = styled.div`
  h4{
      font-size: 14px;
      color: #000000;
      font-family: segoe_ui;
      font-weight: bold;
  }
  span{
    font-size: 14px;
    color: #666666 !important;
    font-family: segoe_ui;
  }
`;
const StyleButton = styled(Button)`
color: #ffffff;
font-size: 12px !important;;
font-family: iCielNovecentosansNormal;
background-color: #333333 !important;
padding: 10px 7px 11px 6px !important;
width: 100%;
 border: 1px solid #cccccc !important;
border-radius: 0 !important;
margin-bottom: 10px;
  &:focus{
    background-color: #f49101 !important;
  }
    &:hover{
        background-color: #f49101 !important;
    }
    
`;
const Infor = (props) => {
  const handleLogOut = () =>{
 
}

  return (
    <div>
      <Container>
        <Row1>
          <Col md="4">
    <img src={user} />
    <h2>weyne owen</h2>
    <p>Architectural designer</p>
    <Row>
    <Col md="6">
        <StyleButton>UPDATE</StyleButton>
        </Col>
        <Col md="6">
        <StyleButton onClick={handleLogOut}>LOG OUT</StyleButton>
        </Col>
    </Row>
          </Col>
          <Col  md="8">
            <Content>
          <h4>Description</h4>
          <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In efficitur risus eros, in aliquam dui posuere eget. Nam iaculis lorem ante, at pellentesque leo ornare eu. Aliquam varius a neque ut ultrices. Suspendisse potenti. Cras at ligula lacus. Cras vel tellus at sapien </span>
            </Content>
            <Row2>
              <Col md="8">
              <h4>Contacts</h4>
              <ul>
                <li>
          <Pin fontSize="24px" color="#666666" style={{}} /> <span>23, 11 Street, Oxfort, London</span>
                </li>
                <li>
         <LogoSkype fontSize="24px" color="#666666" style={{}} /> <span>weyne owen.skype</span>
                </li>
                <li>
          <Phone fontSize="24px" color="#666666" style={{}} /> <span>+1234 567 890</span>
                </li>
                <li>
          <Mail fontSize="24px" color="#666666" style={{}} /> <span>weyne.owen@company.com</span>
                </li>
                <li>
                <Icon icon={socialFacebook} style={{fontSize:"24px",color:"#666666"}} className="iconContact" />
              <LogoTwitter fontSize="24px" color="#666666" className="iconContact" />
              <Icon icon={socialGoogleplus} style={{fontSize:"24px",color:"#666666"}} className="iconContact"/>
              < LogoLinkedin fontSize="24px" color="#666666" className="iconContact" />
             
                </li>
              </ul>
              </Col>
              <Col md="4">
              <h4>Birthday</h4>
              <p>11/06 </p>
              <h4 style={{paddingTop:"50px"}}>Favourite</h4>
              <p>Music, Art, Travel.</p>
              <h4 style={{paddingTop:"50px"}}>Zipcode</h4>
              <p>XYZ123 </p>
                </Col>
            </Row2>
          </Col>
        </Row1>
      </Container>
    </div>
  );
}

export default Infor;