import React ,{useState, useEffect} from 'react';
import {Container,Row, Col, Button} from 'reactstrap';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, CardTitle, CardText } from 'reactstrap';
import styled from 'styled-components';
import BG from "../../images/Slide/wallpapers222.png";
import classnames from 'classnames';
import Infor from "./Infor";
import Information from 'react-ionicons/lib/MdInformation';
import Heart from 'react-ionicons/lib/IosHeart';
import Create from 'react-ionicons/lib/MdCreate';
import ManageCart from "./ManageCart";
const Wrapper = styled.div`
background: transparent;
`;
const AddressBill = styled.div`
    border: 1px solid #cccccc;
    padding-top: 30px;
    padding-left: 20px;
    padding-right: 32px;
    padding-bottom: 107px;
    h3{
        font-size: 14px;
        color: #000000;
        font-family: segoe_ui;
        font-weight: bold;
        margin-bottom: 15px;

    }
    p{
        font-size: 14px  !important;
        color: #666666  !important;
        font-family: segoe_ui  !important;
    
    }
`;
const Banner = styled(Container)`
margin-left: 105px;
margin-right: 104px;
padding-bottom: 100px;
background: white;
position: relative;
img{
    width: 100%;
    height: 424px;
    object-fit: contain;
    padding-right: 10px;
}
div{
 
    h1{
        font-size: 30px;
        color: #ffffff !important;
        font-family: iCielNovecentosansLight;
        margin-bottom: 0;
    }
    p{
        font-size: 13px;
        color: #ffffff ;
        font-family: iCielNovecentosansMedium;
    }
}
`;
const TabProfile =styled(Row)`
    padding-top: 30px;
    .col-md-9{
        padding-right: 0;
    }
    .col-md-3{
        padding-right: 30px;
        padding-left: 0;
    }
`;
const CustomNavLink = styled(NavLink)`
color: #999999;
font-size: 14px;
text-transform: uppercase;
font-weight: bold;
font-family: segoe_ui;
cursor: pointer;
display: flex !important;
align-items: center;
background-color: #e6e6e6;
padding: 19px 30px !important;
&:hover{
    color: #999999; 
}
&.active{
    color: #333333;
}
`;
function Profile({ dataList,...props}) {
    const [activeTab, setActiveTab] = useState('1');

    const toggle = tab => {
      if(activeTab !== tab) setActiveTab(tab);
    }
  
  
    return (
<Wrapper>
<Banner>
    <img src = {BG} />
    <div style={{position: "absolute",left: "5%",top: "325px"}} >
    <h1>USER PROFILE</h1>
    <p>home  &gt;  user profile</p>
    </div>
    <TabProfile>
        <Col md="9">
   <Nav tabs >
        <NavItem>
          <CustomNavLink
            className={classnames({ active: activeTab === '1' })}
            onClick={() => { toggle('1'); }}
            style={{marginLeft: "40px"}} 
            
          >
         <Information fontSize="20px" color="#333333" />   Information
          </CustomNavLink>
        </NavItem>
        <NavItem>
          <CustomNavLink
            className={classnames({ active: activeTab === '2' })}
            onClick={() => { toggle('2'); }}
            style={{marginLeft: "10px"}} 
          >
      <Create fontSize="14px" color="#333333" style={{marginRight: "5px"}}  />  manage cart
           
          </CustomNavLink>
        </NavItem>
        <NavItem>
          <CustomNavLink
            className={classnames({ active: activeTab === '3' })}
            onClick={() => { toggle('3'); }}
            style={{marginLeft: "10px"}} 
          >
         <Heart fontSize="14px" color="#333333" style={{marginRight: "5px"}} />  wish list
          </CustomNavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
         <Infor />
        </TabPane>
        <TabPane tabId="2">
        <ManageCart />
        </TabPane>
      </TabContent>
      </Col>
      <Col md="3">
      <AddressBill>
            <h3>Billing address 1</h3>
            <p>23, 11 Street, Oxfort, London</p>
            <h3 style={{paddingTop: "45px"}}>Billing address 2</h3>
            <p>94, Empires Street, Oxfort, London</p>
      </AddressBill>
      </Col>
   </TabProfile>

</Banner>

</Wrapper>
    )
}

// const mapStateToProps = state => ({
//   dataList: state.addCart
// })
// const mapDispatchToProps = dispatch => ({
 
// })
export default Profile;
