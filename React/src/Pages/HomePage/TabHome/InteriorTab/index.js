import React ,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {Container,Row, Col, Button} from 'reactstrap';
import styled from 'styled-components';
import axios from 'axios';
import search from '../../../../images/Icon/search.png';
import { connect } from "react-redux";
import {Link} from "react-router-dom";

const CustomRow = styled(Row)`
margin-left: -30px !important;
margin-right: -30px !important;
`;

const CustomCol = styled(Col)`
cursor: pointer;
padding-top: 20px;
  p{
    font-size: 18px;
    color: #5b5150;
    font-family: iCielNovecentosansNormal;
    text-transform: uppercase;
    margin-bottom: 0;
  }
  img{
    width: 100%;
    height: 151px;
    object-fit: cover;
    margin-bottom: 10px;
  }
  span{
    margin-top: 20px;
    font-size: 18px;
    color: #f49101;
    font-family: iCielNovecentosansNormal; 
  }
  button{
    background: #cccccc;
    text-transform: uppercase;
    color: #333333;
    font-size: 14px;
    font-family: iCielNovecentosansNormal;
    border-radius: 0;
    margin-top: 10px;
  }
`;
function InteriorTab({ dataList,...props}) {
 
    const [interior,setInterior] = useState([]);
    const [isLoading,setIsLoading] = useState(false);
 const handleClick = () => {
   console.log('click');

 
}
    useEffect(() => {
        const fetchData = async () => {
          setIsLoading(true);
            const result = await axios(`/api/product` ,
            );
            console.log(result.data);
            setInterior(result.data);
            setIsLoading(false);
          };
          fetchData();
         
      }, []);
   
      const data = interior.map((item) => {
        const id = item.id;
        return (
          <CustomCol md = "4" >
        <p key={item.id}>{ item.title } </p>
        <Link to={`/detail/${id}`}>    
         <div className="product" style={{backgroundImage: `url(${item.image1})`,height: "151px",backgroundPosition: "center",
    backgroundSize: "cover"}}>
         <img className="search"  src={search} style={{height: "auto",width: "auto",position: "absolute",top: "40%",
    left: "calc(50% - 16px)"}} /> 
         </div>
         </Link>
            <span>${ item.price }</span>
         
    <Button block  onClick={handleClick}>Add to Cart</Button>
    
          </CustomCol>

        );
        });
    return (
<>
<Container style={{background: "#ffffff",paddingTop: "30px"}}>
{isLoading ? (
        <div>Loading ...</div>
      ) : ( 
    <CustomRow>
    {data}  
    </CustomRow>
      )}
</Container>
</>
    );
}

const mapStateToProps = state => ({
  dataList: state.addCart
})
const mapDispatchToProps = dispatch => ({
 
})
export default connect(mapStateToProps, mapDispatchToProps)(InteriorTab);
