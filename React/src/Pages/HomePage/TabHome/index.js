import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import styled from 'styled-components';
import InteriorTab from './InteriorTab';
import ModelTab from './ModelTab';
import ExteriorTab from './ExteriorTab';

const StyleNavItem = styled(NavItem)`
width: calc(100% / 3);
margin-bottom: 0;
background: #082157;
border-top-left-radius: 0 !important;
border-top-right-radius: 0 !important;
cursor: pointer;
`;
const StyleNavLink = styled(NavLink)`
border-top-left-radius: 0 !important;
border-top-right-radius: 0 !important;
font-size: 18px;
font-family: iCielNovecentosansDemiBold;
color: #ffffff;
&.active{
    background: #f49101 !important;
    color: #ffffff !important;
    border-color: transparent !important;
}
&:hover{
    border-color: transparent !important;
}
`;
const TabHome = (props) => {
  const [activeTab, setActiveTab] = useState('1');

  const toggle = tab => {
    if(activeTab !== tab) setActiveTab(tab);
  }

  return (
    <div>
      <Nav tabs>
        <StyleNavItem>
          <StyleNavLink
           className={classnames({ active: activeTab === '1' })}
            onClick={() => { toggle('1'); }}
          >
            INTERIOR
          </StyleNavLink >
        </StyleNavItem>
        <StyleNavItem>
          <StyleNavLink
            className={classnames({ active: activeTab === '2' })}
            onClick={() => { toggle('2'); }}
          >
          MODELS
          </StyleNavLink >
        </StyleNavItem>
        <StyleNavItem>
          <StyleNavLink
            className={classnames({ active: activeTab === '3' })}
            onClick={() => { toggle('3'); }}
          >
            EXTERIOR
          </StyleNavLink >
        </StyleNavItem>
      </Nav>
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
         <InteriorTab />
        </TabPane>
        <TabPane tabId="2">
         <ExteriorTab />
        </TabPane>
        <TabPane tabId="3">
         <ModelTab />
        </TabPane>
      </TabContent>
    </div>
  );
}

export default TabHome;