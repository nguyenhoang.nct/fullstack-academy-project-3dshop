import React from "react";
import { connect } from "react-redux";
import { Container, Row, Col } from "reactstrap";
import Banner from "./Banner";
import TabHome from "./TabHome";
import Login from "../../Components/Login";
import Gallery from "../../Components/Gallery";
import Blog from "../../Components/Blog";
import Ads from "../../Components/Ads";
import AfterLogin from "../../Components/AfterLogin"
function Home({login}) {
    return (
        <>
            <Banner />
            <Container style={{ background: "#ffffff", paddingTop: "30px" }}>
                <Row>
                    <Col md="9">
                        <TabHome />
                    </Col>
                    <Col md="3">
                        {!login && <Login />}

                        {login && <AfterLogin />}
                        <Gallery />
                        <Blog />
                        <Ads />
                    </Col>
                </Row>
            </Container>
        </>
    );
}

const mapStateToProps = state => ({
    login: state.login
  })
 
  export default connect(mapStateToProps, null)(Home);
