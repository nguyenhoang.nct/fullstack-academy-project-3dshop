import React, { useState, useEffect } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
  Container,
  Row
} from 'reactstrap';
import styled from 'styled-components';
import axios from "axios";
import Slide1 from '../../../images/Slide/wallpapers222.png';
import Slide2 from '../../../images/Slide/589406.jpg';
import Slide3 from '../../../images/Slide/180521.jpg';
import Slide4 from '../../../images/Slide/594203.jpg';
import Slide5 from '../../../images/Slide/382953.jpg';

// const items = [
//   {
//     src: Slide1,
//     altText: 'LEARN MORE',
//     href: 'https://www.youtube.com/watch?v=B3nQKML06Fk'
//   },
//   {
//     src: Slide2,
//     altText: 'LEARN MORE',
//     href: 'https://www.youtube.com/watch?v=fi1xhD7xUIo'
//   },
//   {
//     src: Slide3,
//     altText: 'LEARN MORE',
//     href: 'https://www.youtube.com/watch?v=CgWzaS170yk'
//   },
//   {
//     src: Slide4,
//     altText: 'LEARN MORE',
//     href: '/'
//   },
//   {
//     src: Slide5,
//     altText: 'LEARN MORE',
//     href: '/'
//   },
// ];


const StyleCarouselCaption = styled.p`
cursor: pointer;
    position: absolute;
    bottom: 30px;
    background-color: transparent;
    color: #ffffff;
    font-family: iCielNovecentosansLight;
    text-transform: uppercase;
    left: 45px;
    font-size: 24px;
`;
const Banner = (props) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);
const [data,setData] = useState([]);
useEffect(() => {
  const fetchData = async () => {
  
      const result = await axios(`/api/banner` ,
      );
      console.log(result.data);
      setData(result.data);
     
    };
    fetchData();
   
}, []);
  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === data.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? data.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = data.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.id}
      >
          <a href="/" target="_blank">
          <img style={{width:"100%",paddingLeft: "7px",paddingRight: "7px"}} src={item.link}  />
          </a>
      
        <StyleCarouselCaption>
       LEARN MORE
        </StyleCarouselCaption>
      </CarouselItem>
    );
  });

  return (
   <div id='slide'>

<Container>
    <Row style={{background: "#ffffff"}}>


    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
      
    >
      <CarouselIndicators items={data} activeIndex={activeIndex} onClickHandler={goToIndex} />

      {slides}
      <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
     
    </Carousel>
    </Row>
</Container>
    </div>
  );
}

export default Banner;