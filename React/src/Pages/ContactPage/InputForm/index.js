import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import {
 faEnvelope,faUserAlt
  } from "@fortawesome/free-solid-svg-icons";
  import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from 'styled-components';

const Wrap = styled.div`
position: relative;
`;

const CusInput = styled(Input)`
width: 270px !important;
  color: #999999;
  background: #f7f8f9 !important; 
  font-size: 14px;
  font-family: segoe_ui;
  padding-left: 22px !important;
  padding-top: 20px !important;
  padding-bottom: 20px !important;
  border-radius: 5px !important;
  margin-right: 20px;
  margin-top: 50px;
  margin-bottom: 20px;
  &:focus{
      background: #ffffff !important;;
      box-shadow: none !important;;
  }
`;
const InputArea = styled(Input)`
    color: #999999;
    background: #f7f8f9 !important; 
&:focus{
    background: #ffffff !important;;
    box-shadow: none !important;;
}
`;
const CustomButton = styled(Button)`
    padding: 18px 36px !important; 
    color: #ffffff;
  background: #666666 !important; 
  font-size: 16px;
  font-family: iCielNovecentosansDemiBold;
`;
const InputForm = (props) => {
  return (
    <Form>
        <Wrap style={{display:"flex"}}>
      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
        <CusInput type="text"  id="username" placeholder="Name" />
        <FontAwesomeIcon icon={faUserAlt} color="#999999" style={{fontSize:"14px",position:"absolute",top:"66px",left:"29%"}}/>
      </FormGroup>
      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
        <CusInput type="email" id="email" placeholder="Email" />
        <FontAwesomeIcon icon={faEnvelope} color="#999999" style={{fontSize:"14px",position:"absolute",top:"66px",right:"36%"}}/>
      </FormGroup>
      </Wrap>
     
      
      <FormGroup>
      
        <InputArea type="textarea"  id="exampleText"  rows={12} />
      </FormGroup>
      <CustomButton>SEND MESSAGE</CustomButton>
    </Form>
  );
}

export default InputForm;