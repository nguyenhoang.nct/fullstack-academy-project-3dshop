import React from 'react';
import ReactDOM from 'react-dom';
import {Container,Row, Col, Button} from 'reactstrap';
import styled from 'styled-components';
import Login from '../../Components/Login';
import Gallery from '../../Components/Gallery';
import Blog from '../../Components/Blog';
import Ads from '../../Components/Ads';
import Face from "../../images/Icon/Bigfacebook.png";
import Sky from "../../images/Icon/skypee.png";
import google from "../../images/Icon/Bigg+.png";
import iconPay from "../../images/Pay/icon_paypal.png";
import InputForm from "./InputForm";
const Title = styled.div`
width: 100%;
background: #557c83;
padding-left: 19px;
padding-top: 14px;
padding-bottom: 14px;
background: #082157;
    h2 {
        color: #fff;
        font-size: 18px;
        font-family: iCielNovecentosansDemiBold;
        text-transform: uppercase;
        line-height: 1;
        margin-bottom: 0;
    }
`;

const CustomCol = styled(Col)`
margin-top: 50px;
    button{
        border-radius: 0;
        width: 100%;
        font-size: 18px;
       background: #ffffff;
       color: #999999;
       font-family: segoe_ui;
        p{
           
            margin-bottom: 20px;
        }
    }
    &:nth-of-type(1){
        padding-right: 15px;
        padding-left: 0;
    }
    &:nth-of-type(3){
        padding-right: 0;
        padding-left: 15px;
    }
`;
const CustomRow = styled(Row)`
padding-top: 50px;
    p{
        background: #ffffff;
        color: #999999;
        font-family: segoe_ui;
        font-size: 14px;
        width: 95%;
        margin-bottom: 25px;
    }
    img{
        padding-bottom: 100px;
    }
`;


function Contact() {
    return (
<>

<Container style={{background: "#ffffff",paddingTop: "30px"}}>
    <Row>
        <Col md="9">
  <Title>
        <h2>contact us</h2>
  </Title>
        <Container>
            <Row>
                <CustomCol md="4">
            <Button>
        <img src={Face} style={{paddingTop: "18px"}}  />
        <p style={{paddingTop: "14px"}}>facebook.com/3dshop</p>
            </Button>
                </CustomCol>
                <CustomCol md="4">
                <Button>
        <img src={Sky} style={{paddingTop: "21px"}}/>
        <p style={{paddingTop: "20px"}}>office.3dshop </p>
            </Button>
                    </CustomCol>
                    <CustomCol md="4">
                    <Button>
        <img src={google} style={{paddingTop: "21px"}}/>
        <p style={{paddingTop: "24px"}}>3dshop@gmail.com</p>
            </Button>
                    </CustomCol>
            </Row>
            <CustomRow>
                <p>If you have problems with a PayPal payment or you don't have a possibility to pay in that way, you can always write me and we will solve the problem. Other payment options are possible, e.g.: Western Union, WebMoney, etc. If you have any questions or offers, please contact me:</p>
            <img src={iconPay} />
            </CustomRow>
        </Container>
        <Title>
        <h2>write to us</h2>
  </Title>
        <InputForm />
        </Col>
        <Col md="3">
        <Login />
        <Gallery />
        <Blog />
        <Ads />
        </Col>
    </Row>
</Container>

</>
    );
}

export default Contact;