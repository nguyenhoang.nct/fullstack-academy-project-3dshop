import React from 'react';
import {Container,Row, Col} from 'reactstrap';
import styled from 'styled-components';
import Login from '../../Components/Login';
import Gallery from '../../Components/Gallery';
import Blog from '../../Components/Blog';
import Ads from '../../Components/Ads';
import Content from './Content';
const Title = styled.div`
width: 100%;
background: #557c83;
padding-left: 19px;
padding-top: 14px;
padding-bottom: 14px;
background: #082157;
    h2 {
        color: #fff;
        font-size: 18px;
        font-family: iCielNovecentosansDemiBold;
        text-transform: uppercase;
        line-height: 1;
        margin-bottom: 0;
    }
`;

function FAQ() {
    return (
<>

<Container style={{background: "#ffffff",paddingTop: "30px"}}>
    <Row>
        <Col md="9">
  <Title>
        <h2>Faq</h2>
  </Title>
        <Content />
        </Col>
        <Col md="3">
        <Login />
        <Gallery />
        <Blog />
        <Ads />
        </Col>
    </Row>
</Container>
</>
    );
}

export default FAQ;