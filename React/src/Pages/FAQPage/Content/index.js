import React, { useState } from 'react';
import { Collapse, Button } from 'reactstrap';
import ArrowRight from 'react-ionicons/lib/MdArrowDropright';
import ArrowDown from 'react-ionicons/lib/MdArrowDropdown';
import Answer from 'react-ionicons/lib/IosRedo';
import styled from 'styled-components';
import {FAQ} from "../../../Components/const/common";

const Wap = styled.div`
button{
    font-size: 16px;
    color: #ffffff;
    background: #f49101 !important;
    font-family: iCielNovecentosansMedium;
    text-transform: uppercase;
    padding: 14px 38px;
    border-radius: 0;
    margin-top: 50px;
}
`;

const Wrapper = styled.div`
        border-top: 1px solid #cccccc;
        border-bottom: 1px solid #cccccc;
    h3{
        display: flex;
        font-family: LatoMedium;
        font-size: 18px;
        color: #000000;
        text-transform: uppercase;
        cursor: pointer;
        padding-left: 20px;
        padding-top: 20px;
        padding-bottom: 20px; 
        &:hover{
            color: #ff9900;
        }
        &:focus{
            color: #ff9900;
        }
    }
    
`;
const Hover = styled.div`
    background: #f2f2f2;
    padding-bottom: 1px;
    padding-left: 20px;
    padding-top: 24px;
    padding-right: 74px;
    padding-bottom: 55px;
    margin-bottom: 0;
`;
const Content = (props) => {
    const [activeIndex, setActiveIndex] = useState(null);
  const [isOpen, setIsOpen] = useState(false);

  const data = FAQ.map((item,index) => {
  return (

        <Wrapper key={index} style={{paddingTop: "10px"}}>
      <h3 onClick={e => setActiveIndex(
                    activeIndex === index ? null : index)}> <ArrowRight style={{marginRight: "10px",display: activeIndex === index ? "none" : "block"}} fontSize="20px" color="#000000" />
      <ArrowDown style={{marginRight: "10px",display: activeIndex === index ? "block" : "none"}} fontSize="20px" color="#000000" />
      {item.question} </h3>
      <Collapse isOpen={activeIndex === index} data-parent="#accordion">
          <Hover>
              <p>
          <Answer  fontSize="24px" color="#999999" /> {item.answer} <a href="/">{item.tag}</a>
          </p>
          </Hover>
      </Collapse>
      </Wrapper>
  );

});

return (
<Wap >
{data}
<div style={{textAlign: "right"}}>
<Button>continue</Button>
</div>

</Wap>
);
}
export default Content;




