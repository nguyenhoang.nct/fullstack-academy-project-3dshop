// import React ,{useState, useEffect} from 'react';
// import {Container,Row, Col, Button} from 'reactstrap';
// import styled from 'styled-components';
// import axios from 'axios';
// import search from '../../../images/Icon/search.png';
// import { connect } from "react-redux";
// import {  addToCart } from '../../../redux/action/action';
// import PagInter from "./Pagination";

// const CustomRow = styled(Row)`
// .col-md-4{
//   &:first-of-type{
//     padding-left: 0;
//   }
//   &:last-of-type{
//     padding-right: 0;
//   }
// }
// `;

// const CustomCol = styled(Col)`
// cursor: pointer;
// p{
//   font-size: 18px;
//   color: #5b5150;
//   font-family: iCielNovecentosansNormal;
//   text-transform: uppercase;
//   margin-bottom: 0;
// }
// img{
//   width: 100%;
//   height: 151px;
//   object-fit: cover;
//   margin-bottom: 10px;
// }
// span{
//   margin-top: 20px;
//   font-size: 18px;
//   color: #f49101;
//   font-family: iCielNovecentosansNormal; 
// }
// button{
//   background: #cccccc;
//   text-transform: uppercase;
//   color: #333333;
//   font-size: 14px;
//   font-family: iCielNovecentosansNormal;
//   border-radius: 0;
//   margin-top: 10px;
// }
// `;
// function Content({items,addToCart}) {

 
//   const [product,setProduct] = useState( [] );
  
//   useEffect(() => {
//     const fetchData = async () => {
//         const result = await axios(`/api/product` ,
//         );  
//         console.log(result.data);
//         setProduct(result.data);
    
//       };
//       fetchData();
    
//   }, []);
//   function handleClick(id,name,image,price,model){
//     const newItem = {
//       id: id,
//       name: name,
//       image: image,
//       price: price,
//       model: model
//   };
//     addToCart(newItem);
     
//  }
 
//     const dataProduct = product.map((item) => {
//       return (
//         <CustomCol md = "4" >
//       <p key={item.id}>{ item.title } </p> 
//        <div className="product" style={{backgroundImage: `url(${item.image1})`,height: "151px",backgroundPosition: "center",
//   backgroundSize: "cover"}}>
//        <img className="search"  src={search} style={{height: "auto",width: "auto",position: "absolute",top: "calc(50% - 17px)",
//   left: "calc(50% - 16px)"}} /> 
//        </div>
//           <span>${ item.price }</span>
       
//   <Button block onClick={()=>{handleClick(item.id,item.title,item.image1,item.price,item.Model)}}>Add to Cart</Button>
  
//         </CustomCol>

//       );
//       });
//   return (
// <>
// <Container style={{background: "#ffffff",paddingTop: "50px"}}>
//   <CustomRow>
//   {dataProduct}  

//   </CustomRow>
// </Container>
// <PagInter />
// </>
//   );
// }

// const mapStateToProps = state => ({
//   items: state.items
// })
// const mapDispatchToProps = dispatch => ({
  
//   addToCart: (id)=>{dispatch(addToCart(id))}
// })
// export default connect(mapStateToProps, mapDispatchToProps)(Content);



import React ,{useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import {Container,Row, Col, Button} from 'reactstrap';
import styled from 'styled-components';
import axios from 'axios';
import search from '../../../images/Icon/search.png';
import { connect } from "react-redux";
import {Link} from "react-router-dom";
import {  addToCart } from '../../../redux/action/action';
import  Pagination  from "@material-ui/lab/Pagination";
import usePagination from "./Pagination";

const CustomRow = styled(Row)`

.col-md-4{
  &:first-of-type{
    padding-left: 0;
  }
  &:nth-of-type(4){
    padding-left: 0;
  }
  &:nth-of-type(3){
    padding-right: 0;
  }
}
`;

const CustomCol = styled(Col)`
cursor: pointer;
padding-top: 20px;
p{
  font-size: 18px;
  color: #5b5150;
  font-family: iCielNovecentosansNormal;
  text-transform: uppercase;
  margin-bottom: 0;
}
img{
  width: 100%;
  height: 151px;
  object-fit: cover;
  margin-bottom: 10px;
}
span{
  margin-top: 20px;
  font-size: 18px;
  color: #f49101;
  font-family: iCielNovecentosansNormal; 
}
button{
  background: #cccccc;
  text-transform: uppercase;
  color: #333333;
  font-size: 14px;
  font-family: iCielNovecentosansNormal;
  border-radius: 0;
  margin-top: 10px;
}
`;

 function Content({items,addToCart}) {
  let [page, setPage] = useState(1);
  const PER_PAGE = 4;
  const [data,setData] = useState( [] );
  useEffect(() => {
      const fetchData = async () => {
          const result = await axios(`/api/product` ,
          );        
          setData(result.data);
        };
        fetchData();
      
    }, []);
  const count = Math.ceil(data.length / PER_PAGE);

  const _DATA = usePagination(data, PER_PAGE);

  const handleChange = (e, p) => {
    setPage(p);
    _DATA.jump(p);
    window.scrollTo(0, 0);
  };
  function handleClick(id,name,image,price,model){
    const newItem = {
      id: id,
      name: name,
      image: image,
      price: price,
      model: model
  };
    addToCart(newItem);
 }
const clickhandle = () =>{
  window.scrollTo(0, 0);
}

  return (
    <Container>
        <CustomRow>
        {_DATA.currentData().map(item => {
          const id = item.id
          return (
            <CustomCol md = "4" >
            <p key={item.id}>{ item.title } </p> 
            <Link to={`/detail/${id}`} onClick={clickhandle}>
             <div className="product" style={{backgroundImage: `url(${item.image1})`,height: "151px",backgroundPosition: "center",
        backgroundSize: "cover"}}>
             <img className="search"  src={search} style={{height: "auto",width: "auto",position: "absolute",top: "40%",
        left: "calc(50% - 16px)"}} /> 
             </div>
             </Link>
                <span>${ item.price }</span>
              
        <Button block  onClick={()=>{handleClick(item.id,item.title,item.image1,item.price,item.Model)}}>Add to Cart</Button>
        {/* <Button block >Add to Cart</Button> */}
              </CustomCol>
          );
        })}
      </ CustomRow>

      <Pagination
        count={count}
        size="large"
        page={page}
        variant="outlined"
        shape="rounded"
        onChange={handleChange}
      />
    </Container>
  );
}

const mapStateToProps = state => ({
  items: state.items
})
const mapDispatchToProps = dispatch => ({
  
  addToCart: (id)=>{dispatch(addToCart(id))}
})
export default connect(mapStateToProps, mapDispatchToProps)(Content);