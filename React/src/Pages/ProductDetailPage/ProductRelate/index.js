import React, { useState } from 'react';
import {Container,Row, Col,Button} from 'reactstrap';
import styled from 'styled-components';
import search from '../../../images/Icon/search.png';
import {PRODUCT} from "../data";
const Title = styled.div`
    position: relative;
    text-align:center;
    h2{
        font-size: 24px;
        font-family: iCielNovecentosansNormal ;
        color: #333333;
        padding-bottom: 12px;
       
        &::after{
            position: absolute;
            content: "";
            width: 27%;
            height: 3px;
            background-color: #f49101;
            bottom:0;
            left: 36.5%;
        }
    }
`;

const CustomCol = styled(Col)`
cursor: pointer;
margin-top: 50px;
  p{
    font-size: 18px;
    color: #5b5150;
    font-family: iCielNovecentosansNormal;
    text-transform: uppercase;
    margin-bottom: 0;
  }
  img{
    width: 100%;
    height: 151px;
    object-fit: cover;
    margin-bottom: 10px;
  }
  span{
    margin-top: 20px;
    font-size: 18px;
    color: #f49101;
    font-family: iCielNovecentosansNormal; 
  }
  button{
    background: #cccccc;
    text-transform: uppercase;
    color: #333333;
    font-size: 14px;
    font-family: iCielNovecentosansNormal;
    border-radius: 0;
    margin-top: 10px;
    padding-top: 10px !important; 
    padding-bottom: 7px !important;
  }
`;
function RelateProduct() {
    const data1 = PRODUCT.map((item) => {
        return (
          <CustomCol md = "4" >
        <p key={item.id}>{ item.title } </p>
         <div className="product" style={{backgroundImage: `url(${item.img})`,height: "151px",backgroundPosition: "center",
    backgroundSize: "cover"}}>
         <img className="search"  src={search} style={{height: "auto",width: "auto",position: "absolute",top: "calc(50% - 17px)",
    left: "calc(50% - 16px)"}} /> 
         </div>
            <span>${ item.price }</span>
         
    <Button block >Add to Cart</Button>
    
          </CustomCol>

        );
        });
    return (
<Container style={{marginTop:"100px",paddingBottom:"100px"}}>
  <Title>
  <h2>RELATED PRODUCTS</h2>    
  </Title>      
  <Row style={{marginRight: "-25px",marginLeft: "-25px"}}>
   {data1}
  </Row>
</Container>
    );
}

 
  export default RelateProduct;
