import React from 'react';
import ReactDOM from 'react-dom';
import {Container,Row, Col, Button} from 'reactstrap';
import styled from 'styled-components';
import P1 from "../../../images/ProductDetail/1.jpg";
import P1_1 from "../../../images/ProductDetail/1-1.jpg";
import P1_2 from "../../../images/ProductDetail/1-2.jpg";
import P1_3 from "../../../images/ProductDetail/1-3.jpg";
import P1_4 from "../../../images/ProductDetail/1-4.jpg";
import Cart from 'react-ionicons/lib/IosCart'
const StyleCon = styled(Container)`
border-bottom: 1px solid #cccccc;
    span{
        font-size: 18px;
        font-family: iCielNovecentosansDemiBold ;
        color: #f49101;
    }
     p{
        font-size: 18px;
        font-family: iCielNovecentosansNormal ;
        color: #5b5150;
    }
    button{
        font-size: 18px;
        font-family: iCielNovecentosansNormal ;
        color: #ffffff;
        text-transform: uppercase;
        background: #f49101;
        padding: 20px 17px;
        border-radius: 0;
    }
    a{
        font-size: 14px;
        font-family: segoe_ui ;
        color: #f49101;
        &:hover{
            color: #f49101;  
        }
    }
`;
const StyleRow = styled(Row)`
    img{
        width: 170px;
        height: 190px;
        &:hover{
            border: 1px solid #f49101;
        }
    }
    padding-bottom: 60px;
    border-bottom: 1px solid #cccccc;
`;
const RowCustom = styled(Row)`
    margin-top: 30px;
    padding-bottom: 15px;
    & .col-md-4{
        &:nth-of-type(1){
            padding-top: 17px;
        }
        &:nth-of-type(3){
            padding-top: 40px;
        }
    }
`;
const Wrapper = styled.div`
padding-bottom: 40px;
border-bottom: 1px solid #cccccc;
    h2{
        margin-top: 30px;
        font-size: 24px;
        font-family: iCielNovecentosansNormal ;
        color: #999999;
        padding-bottom: 23px;
        position: relative;
        margin-bottom: 25px;
        &::after{
            position: absolute;
            content: "";
            width: 17%;
            height: 3px;
            background-color: #f49101;
            bottom:0;
            left:0;
        }
    }    
    p{
        margin-bottom: 10px;
        line-height: 1.5;
        font-size: 14px;
        font-family: segoe_ui ;
        color: #333333; 
    }
    h4{
        font-size: 14px;
        font-family: segoe_ui ;
        color: #666666; 
        padding-top: 35px;
        margin-bottom: 35px;
    }
    a{
        font-size: 14px;
        font-family: segoe_ui ;
        color: #082157; 
      &:hover{
        text-decoration: none;
      }
    }
`;
function Content() {
    return (
        <>      
<StyleCon>
    <StyleRow>
        <Col md="6" style={{paddingLeft: "0"}}>
        <img src ={P1} className="w-100 h-100" style={{marginTop:"35px"}} />
        </Col>
        <Col md="6">
        <Row style={{paddingTop: "87px",marginLeft:"0",marginRight:"0"}}>
        <Col md="6">
        <img src ={P1_1} />
        </Col>
        <Col md="6">
        <img src ={P1_2} />
        </Col>
        </Row>
        <Row style={{paddingTop: "13px",marginLeft:"0",marginRight:"0"}}>
        <Col md="6">
        <img src ={P1_3} />
        </Col>
        <Col md="6">
        <img src ={P1_4} />
        </Col>  
        </Row>
        </Col>
    </StyleRow>
    <RowCustom>
        <Col md="4" style={{paddingLeft: "0"}}>
            <span>€150</span>
            <p>DOWNLOAD ONLY</p>
        </Col>
        <Col md="4" style={{textAlign: "center"}}>
            <Button>
            <Cart  fontSize="18px" color="#ffffff" /> add to cart
            </Button>
        </Col>
        <Col md="4" style={{display: "flex",justifyContent: "flex-end",paddingRight: "0"}}>
            <a href="/"> Ask a question about this product &gt; </a>
        </Col>
    </RowCustom>
</StyleCon>

<Wrapper>
<h2>DESCRIPTION</h2>

<p><b>3DARCINTERIOR VOL 17</b> consists of 666 interior scenes with various designs and styles. </p>
<p>&nbsp; This collection is designed for architectural visualizations made in 3ds MAX. Consists of 552 fully modeled and textured 3d model interiors with complete lighting and cameras setups for every scene. </p>
<p>&nbsp; All presented renders are with postproduction.</p>
<p>&nbsp; Minimum system specification: Quad Core PC with 4GB (8GB recommended) of ram and 64bit system.</p>
<b>Total size : 13.0 GB in 552 files (.rar) (Unrar : 73.9 GB).</b>
<p>&nbsp; 3DArcShop provides royalty free 3d models, our libraries can be used as part of commercial pictures & designs at no extra charge.</p>
<b>This collection contains :</b>
<h4>I - PUBLIC WORKS</h4>
<p>01 - Hotel - Lobby &amp Reception : <a href="#">CLICK TO PAGE 01</a></p>
<p>02 - Hotel - Restaurant : <a href="#">CLICK TO PAGE 02</a></p>
<p>03 - Hotel - Restaurant VIP : <a href="#">CLICK TO PAGE 03</a></p>
<p>04 - Coffee : <a href="#">CLICK TO PAGE 04</a></p>
<p>05 - Hotel - Room : <a href="#">CLICK TO PAGE 05</a></p>
<h4>II - INTERIOR DESIGN ALLIANCE</h4>
<p>15 - Hotel : <a href="#">CLICK TO PAGE 15</a></p>
<p>16 - Restaurant : <a href="#">CLICK TO PAGE 16</a></p>
<p>17 - Interior Design : <a href="#">CLICK TO PAGE 17</a></p>
<p>… and lots of other interior scenes, you can see more images below or download attached catalogue.</p>
<br />
<b>Available formats :</b>
<div>
    <p style={{paddingLeft:"35px"}}>-  V-Ray *.max - 1.5 or higher - with textures and shaders</p>
    <p style={{paddingLeft:"35px"}}>-  *.max (3ds Max 2010 or higher, 3DS MAX 2012 64bit recommended)</p>
</div>
</Wrapper>
                        
        </>
    );
}

export default Content;
