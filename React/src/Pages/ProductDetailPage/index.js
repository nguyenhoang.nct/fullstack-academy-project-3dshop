import React, {useState,useEffect} from 'react';
import {Container,Row, Col} from 'reactstrap';
import styled from 'styled-components';
import Login from '../../Components/Login';
import Gallery from '../../Components/Gallery';
import Blog from '../../Components/Blog';
import Ads from '../../Components/Ads';
import Content from "./Content";
import Comment from "./Comments";
import RelateProduct from "./ProductRelate";
import {connect} from "react-redux";
import axios from "axios";
import _find from "lodash/find";
const Title = styled.div`
width: 100%;
background: #557c83;
padding-left: 19px;
padding-top: 14px;
padding-bottom: 14px;
background: #082157;
    h2 {
        color: #fff;
        font-size: 18px;
        font-family: iCielNovecentosansDemiBold;
        text-transform: uppercase;
        line-height: 1;
        margin-bottom: 0;
    }
`;

const ProductDetail = ({
    match: {
      params: { id },
    },
    items,
    productList,
    addedItems,
  }) => {
 const [data, setData] = useState([]);
 useEffect(() => {
    const fetchData = async () => {
        const result = await axios(`/api/product` ,
        )
        setData(result.data);
      };
      fetchData();
     
  }, []);
  console.log(data);
  let lstItem = _find(data, function (o) {
    return o.id == id;
  });
  console.log(lstItem);
    return (
        <>
           
<Container style={{background: "#ffffff",paddingTop: "30px"}}>
    <Row>
        <Col md="9">
  <Title>
        <h2>Interior</h2>
  </Title>
  <Content />
  <Comment />
  <RelateProduct />
        </Col>
        <Col md="3">
        <Login />
        <Gallery />
        <Blog />
        <Ads />
        </Col>
    </Row>
</Container>
                        
        </>
    );
}
const mapStateToProps = (state) => {
    return {
      items: state.items,
      productList: state.productList,
      addedItems: state.addedItems,
    };
  };
  
  export default connect(mapStateToProps, null)(ProductDetail);

