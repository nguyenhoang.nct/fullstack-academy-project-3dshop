import React from 'react';
import ReactDOM from 'react-dom';
import {Container,Row, Col, Button, Form, FormGroup, Label, Input} from 'reactstrap';
import styled from 'styled-components';
import {
    faEnvelope,faUserAlt,faGlobe
     } from "@fortawesome/free-solid-svg-icons";
     import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import txt from "../../../images/Icon/text.png"
import Ava1 from "../../../images/User/avatar1.PNG";
import Ava2 from "../../../images/User/avatar2.PNG";
import Ava3 from "../../../images/User/avatar3.PNG";
const Col2 = styled(Col)`
display: flex;
justify-content: flex-end;
align-items: center;
p{
    font-size: 16px;
    font-family: segoe_ui ;
    color: #a1b1bc;
    padding-top: 12px;
    margin-left: 20px;
}
`;
const Col1 = styled(Col)`
    h2{
        padding-top: 14px;
        font-size: 30px;
        font-family: iCielNovecentosansNormal ;
        color: #333333;
        padding-bottom: 12px;
        position: relative;
        text-transform: uppercase;
        &::after{
            position: absolute;
            content: "";
            width: 46%;
            height: 3px;
            background-color: #f49101;
            bottom:0;
            left:0;
        }
    }
`;
const Row2 = styled(Row)`
padding-top: 30px;
margin-top: 40px;
margin-bottom: 25px;
border:1px solid #cccccc;
 .col-md-2{
    padding-left: 0;
}
    & .col-md-10{
        padding-right: 30px;
        padding-left: 0;
        div{
            position: relative;
            p{
                margin-top: 0;
                margin-bottom: 0;
                font-size: 18px;
                font-family: segoe_ui ;
                color: #333333;
                font-weight: bold;
                padding-bottom: 0;
            }
            span{
                font-size: 13px;
                font-family: segoe_ui ;
                color: #999999;
            }
            button{
                position: absolute;
                top: 0;
                right: 0;
                font-size: 14px;
                font-family: iCielNovecentosansDemiBold ;
                color: #ffffff;
                background: #082157;
                padding: 7px 11px;
                &:hover{
                    background: #f49101;
                }
                &:focus{
                    background: #f49101;
                }
            }
        }
        p{
            margin-top: 10px;
                font-size: 14px;
                font-family: segoe_ui ;
                color: #999999; 
                padding-bottom: 15px;
        }
    }
`;

const Wrap = styled.div`
position: relative;
`;

const CusInput = styled(Input)`
width: 260px !important;
  color: #999999;
  background: #f7f8f9 !important; 
  font-size: 14px;
  font-family: segoe_ui;
  padding-left: 22px !important;
  padding-top: 20px !important;
  padding-bottom: 20px !important;
  border-radius: 5px !important;
  margin-right: 20px;
  margin-top: 50px;
  margin-bottom: 20px;
  &:focus{
      background: #ffffff !important;;
      box-shadow: none !important;;
  }
`;
const InputArea = styled(Input)`
    color: #999999;
    background: #f7f8f9 !important; 
&:focus{
    background: #ffffff !important;;
    box-shadow: none !important;;
}
`;
const CustomButton = styled(Button)`
    padding: 18px 36px !important; 
    color: #ffffff;
  background: #082157 !important; 
  font-size: 16px;
  font-family: iCielNovecentosansDemiBold;
`;
function Comment() {
    return (
        <>
           
<Container style={{background: "#ffffff",paddingTop: "16px"}}>
 <Row>
     <Col1>
     <h2>2 comments</h2>
     </Col1>
     <Col2>
     <img src={txt} />
     <p>Leave a Comment</p>
     </Col2>
 </Row>
 <Row2 >
        <Col md="2">
        <img src={Ava1} />
        </Col>
        <Col md="10">
        <div>
            <p>Miha Yesterday</p>
            <span>March 23, 2017 at 7:59 pm</span>
           <Button>Reply</Button>
        </div>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque  urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus.
        </p>
        </Col>
 </Row2>
 <Row2 >
        <Col md="2">
        <img src={Ava2} />
        </Col>
        <Col md="10">
        <div>
            <p>Miha Cocacola</p>
            <span>March 23, 2017 at 7:59 pm</span>
           <Button>Reply</Button>
        </div>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Lorem ipsum dolor sit amet!!!
        </p>
        </Col>
 </Row2>
 <Row2 style={{marginLeft: "50px",paddingTop: "15px"}} >
        <Col md="2">
        <img src={Ava3} />
        </Col>
        <Col md="10">
        <div>
            <p>Miha Vinataba</p>
            <span>March 23, 2017 at 7:59 pm</span>
           <Button>Reply</Button>
        </div>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Lorem ipsum dolor sit amet!!!
        </p>
        </Col>
 </Row2>
</Container>
       <Wrap>
           <h2>Leave A Comment</h2>
           <Form>
        <Wrap style={{display:"flex"}}>
      <FormGroup >
        <CusInput type="text"  id="username" placeholder="Name" />
        <FontAwesomeIcon icon={faUserAlt} color="#999999" style={{fontSize:"14px",position:"absolute",top:"66px",left:"29%"}}/>
      </FormGroup>
      <FormGroup >
        <CusInput type="email" id="email" placeholder="Email" />
        <FontAwesomeIcon icon={faEnvelope} color="#999999" style={{fontSize:"14px",position:"absolute",top:"66px",right:"36%"}}/>
      </FormGroup>
      <FormGroup >
        <CusInput type="text" id="website" placeholder="Website" />
        <FontAwesomeIcon icon={faGlobe} color="#999999" style={{fontSize:"14px",position:"absolute",top:"66px",right:"25px"}}/>
      </FormGroup>
      </Wrap>     
      <FormGroup>
        <InputArea type="textarea"  id="exampleText"  rows={12} />
      </FormGroup>
      <CustomButton>SUBMIT COMMENT</CustomButton>
    </Form>
       </Wrap>

        </>
    );
}

export default Comment;
