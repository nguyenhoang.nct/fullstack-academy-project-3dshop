import React from 'react';
import {Container,Row, Col} from 'reactstrap';
import styled from 'styled-components';
import Login from '../../Components/Login';
import Gallery from '../../Components/Gallery';
import Blog from '../../Components/Blog';
import Ads from '../../Components/Ads';
import CheckoutCart from "./CheckoutCart";


function Cart() {
    return (
<>

<Container style={{background: "#ffffff",paddingTop: "30px"}}>
    <Row>
        <Col md="9">
        <CheckoutCart />

        </Col>
        <Col md="3">
        <Login />
        <Gallery />
        <Blog />
        <Ads />
        </Col>
    </Row>
</Container>
</>
    );
}

export default Cart;