import React ,{useEffect, useState} from 'react';
import { Button, Col, Container, Input, Row, Table } from 'reactstrap';
import styled from 'styled-components';
// import {Check_Cart} from "./common";
import { connect } from 'react-redux';

import {removeItem,addQuantity,subtractQuantity} from "../../../redux/action/action";


const Address = styled.div`

padding-left: 170px;
padding-right: 130px;
}
h3{
    color: #363636;
    font-size: 18px;
    font-family: segoe_ui; 
    font-weight: bold;
    padding-top: 50px;
    padding-bottom: 12px;
}
.form-control{
    width: 45%;
    color: #363636;
    font-size: 14px;
    font-family: segoe_ui; 
    padding-left: 20px;
    padding-top: 12px;
    padding-bottom: 10px;
    height: auto;
    background: #eeeeee;
}
a{
    display: flex;
    text-align: center;
    align-items: flex-end;
    color: #363636;
    font-size: 14px;
    font-family: segoe_ui;
    padding-left:20px;
    text-decoration: underline;
}
`;

const Stylediv = styled.div`
margin-top: 30px;
border: 1px solid #dddddd;
padding-left: 45px;
padding-right: 45px;
    h3{
        position: relative;
        padding-bottom: 21px;
   
        &::after{
            position: absolute;
            content: "";
            width: 100%;
            height: 1px;
            background-color: #dddddd;
            left: 0;
            bottom: 0;
        }
    }
    p{
        
        color: #666666;
        font-size: 14px;
        font-family: segoe_ui;
        margin-bottom: 15px;
    }
    button{
        margin-top: 30px;
        color: #363636;
        font-size: 14px;
        text-transform: uppercase;
        font-family: iCielNovecentosansDemiBold; 
        color: #ffffff;
        margin-bottom: 20px;
    }
`;
const CheckoutAddress = ( {items,total,
  removeItem,addQuantity,subtractQuantity}) => {
    const [hover, setHover] = useState(false);
    const [checked, setChecked] = useState(false);
    const[data, setData] = useState();
    const handleRemove = (id) =>{
      removeItem(id);
    }
    const handleSubQuantity = (id) =>{
      subtractQuantity(id);
    }
    const handleAddQuantity = (id) =>{
      addQuantity(id);
    }
    useEffect(() => {
      setData(items)
    }, [items])

return (
  <>
  <Address>
      <h3>Choose a billing address</h3>
      <div style={{display:"flex"}}>
      <Input type="select" name="select" id="exampleSelect">
          <option>Your Billing Address 1</option>
          <option>Your Billing Address 2</option>
          <option>Your Billing Address 3</option>
          <option>Your Billing Address 4</option>
          <option>Your Billing Address 5</option>
        </Input>
        <a href="#">Add a new address</a>
      </div>
      <Stylediv>
          <h3>YOUR BILLING ADDRESS</h3>
          <p style={{paddingTop: "25px"}}>23, </p>
          <p>11-Street, </p>
          <p>Oxfort, </p>
          <p>London, </p>
        <Button>Update</Button>
      </Stylediv>

    
  </Address>
 

</>
)

}

const mapStateToProps = (state)=>{
  return{
    items: state.addedItems,
      total: state.total
  }
}
const mapDispatchToProps = (dispatch) => {
  return{
      removeItem: (id) => {dispatch(removeItem(id))},
      addQuantity: (id) => {dispatch(addQuantity(id))},
      subtractQuantity: (id) => {dispatch(subtractQuantity(id))}
      
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(CheckoutAddress)



