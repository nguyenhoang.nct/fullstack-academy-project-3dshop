import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import styled from 'styled-components';
import Divine from "../../../images/Icon/divine.png";
import Divine1 from "../../../images/Icon/divine1.png";
import actDivine from "../../../images/Icon/divine_active.png";
import Content from "./Content";
import Forward from 'react-ionicons/lib/MdArrowRoundForward';
import CheckoutPayment from "../CheckoutPayment";
import CheckoutAddress from "../CheckoutAddress"
const StyleNavItem = styled(NavItem)`
width: calc(100% / 3);
margin-bottom: 0;
background: #082157;
border-top-left-radius: 0 !important;
border-top-right-radius: 0 !important;
cursor: pointer;
position: relative;

`;
const StyleNavLink = styled(NavLink)`
border-top-left-radius: 0 !important;
border-top-right-radius: 0 !important;
font-size: 18px;
font-family: iCielNovecentosansDemiBold;
color: #ffffff;
    padding: 0 1rem !important;
    padding-top: 5.5px !important;
    padding-bottom: 5.5px !important;
text-transform: uppercase;
position: relative;

&.active{
    background: #f49101 !important;
    color: #ffffff !important;
    border-color: transparent !important;
 
}
&:hover{
    border-color: transparent !important;
}


`;
const CheckoutCart = (props) => {
  const [activeTab, setActiveTab] = useState('1');

  const toggle = tab => {
    if(activeTab !== tab) setActiveTab(tab);
  }
const changeTab = () =>{
  if(activeTab === '1'){
    toggle('2');
  }
  if(activeTab === '2'){
    toggle('3');
  }
}
  return (
    <div>
      <Nav tabs>
        <StyleNavItem>
          <StyleNavLink
           className={classnames({ active: activeTab === '1' })}
            onClick={() => { toggle('1'); }}
          >
            1. Shopping cart
           <img src ={actDivine} style={{position: "absolute",right: "-1px",
    top: "-1px",display: activeTab === '1' ? "block" : "none" }} />
       <img src ={Divine1} style={{position: "absolute",right: "-1px",
    top: "-1px",display: activeTab === '3' ? "block" : "none" }} />
          </StyleNavLink >
        </StyleNavItem>
        <StyleNavItem>
          <StyleNavLink
            className={classnames({ active: activeTab === '2' })}
            onClick={() => { toggle('2'); }}
          >
            <img src ={Divine} style={{position: "absolute",left: "-21px",
    top: "-1px",display: activeTab === '2' ? "block" : "none" }} />  
          2. address
          <img src ={actDivine} style={{position: "absolute",right: "-1px",
    top: "-1px",display: activeTab === '2' ? "block" : "none" }} />  
          </StyleNavLink >
        </StyleNavItem>
        <StyleNavItem>
          <StyleNavLink
            className={classnames({ active: activeTab === '3' })}
            onClick={() => { toggle('3'); }}
          >
           <img src ={Divine1} style={{position: "absolute",left: "-20px",
    top: "-1px",display: activeTab === '1' ? "block" : "none" }} />
        <img src ={Divine} style={{position: "absolute",left: "-20px",
    top: "-1px",display: activeTab === '3' ? "block" : "none" }} />     
            3. payment
           
          </StyleNavLink >
        </StyleNavItem>
      </Nav>
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
       <Content />
        </TabPane>
        <TabPane tabId="2">
       <CheckoutAddress />
        </TabPane>
        <TabPane tabId="3">
         <CheckoutPayment />
        </TabPane>
      </TabContent>
      <div style={{marginTop: "40px"}}>
    <Button className="backShop">continue to shopping</Button>
    <Button  className="checkOut"  onClick={changeTab}>proceed to checkout  <Forward style={{cursor:"pointer"}}  fontSize="18px" color="#ffffff" /> </Button>
  </div>
    </div>
  );
}

export default CheckoutCart;


