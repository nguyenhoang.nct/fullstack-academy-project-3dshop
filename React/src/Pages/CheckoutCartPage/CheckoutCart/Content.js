import React ,{useEffect, useState} from 'react';
import { Button, Input, Table } from 'reactstrap';
import styled from 'styled-components';
// import {Check_Cart} from "./common";
import { connect } from 'react-redux';
import ArrowUp from 'react-ionicons/lib/IosArrowUp';
import ArrowDown from 'react-ionicons/lib/IosArrowDown';
import Trash from 'react-ionicons/lib/MdTrash';
import Refresh from 'react-ionicons/lib/MdRefresh';


import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faMinus, faTrash } from "@fortawesome/free-solid-svg-icons";
import {removeItem,addQuantity,subtractQuantity} from "../../../redux/action/action";
const Wap =styled.div`
   background: #e3e3e3;
   text-align: center;
   width: 79px;
   img{
    width: 79px;
    height: 79px;
    object-fit: cover;
   }
`;

const CustomTable = styled(Table)`
margin-top: 30px;
   tbody{
       td{
        text-align: -webkit-center;
         justify-content: center;
         vertical-align: middle;
       }
   }
   thead{
       th{
        text-align: center;
       }
   }
`;
const IconUp = styled.div`
  width: 13px;
  height: 13px;
  background: #cccccc;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
   top: 0;
   right: 0
`;
const IconDown = styled.div`
   width: 13px;
   height: 13px;
   background: #999999;
   display: flex;
   justify-content: center;
   align-items: center;
   position: absolute;
   top: 13px;
   right: 0
`;
const Wrap = styled.div`
margin-left: 30px;
margin-right: 20px;
   position: relative;
   border: 1px solid #cccccc;
   width: 55px;
   height: 27px;
   text-align: start;
   align-items: baseline;
    display: flex;
   p{
    color: #3b3b3b;
    font-size: 14px;
    font-family: segoe_ui;
    padding-left: 7px;
   }
`;
const Total = styled.div`
text-align: right;
margin-top: 30px;
   h3{
    color: #000000;
    font-size: 24px;
    font-weight: bold;
    font-family: segoe_ui;
   }
   span{
    padding-left: 50px;
     color: #3b3b3b;
    font-size: 24px;
    font-family: segoe_ui;
   }
`;
const CheckCart = ( {items,total,
  removeItem,addQuantity,subtractQuantity}) => {
    const [hover, setHover] = useState(false);

    const[data, setData] = useState();
    const handleRemove = (id) =>{
      removeItem(id);
    }
    const handleSubQuantity = (id) =>{
      subtractQuantity(id);
    }
    const handleAddQuantity = (id) =>{
      addQuantity(id);
    }
    useEffect(() => {
      setData(items)
    }, [items])

return (
  <>
    <CustomTable bordered hover>
    <thead>
      <tr>
        <th>Image</th>
        <th>Product Name</th>
        <th>Model</th>
        <th>Unit Prize</th>
        <th>Quantity</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody>
    {data && data.length > 0 && data.map((item,index) => (
    <tr key={item.id}>
        
    <td><Wap>
    <img src={item.image} />
        </Wap> </td>
    <td>{item.name} </td>
    <td>{item.model} </td>
    <td>${item.price}.00 </td>
    <td style={{ display: "flex",height: "120px",alignItems: "center"}}>
        <Wrap>
        <IconDown>
        <ArrowDown style={{cursor:"pointer"}} onClick={()=>{handleSubQuantity(item.id)}}  fontSize="8.1px" color="#3b3b3b" /> 
        </IconDown> 
      <p>  {item.quantity} </p>
       <IconUp>
       <ArrowUp style={{cursor:"pointer"}} onClick={()=>{handleAddQuantity(item.id)}}  fontSize="8.1px" color="#3b3b3b" />  
       </IconUp>
        </Wrap>   
        <div  onMouseEnter = {()=>{setHover(true)}} onMouseLeave ={()=>{setHover(false)}}>
        <Refresh style={{cursor:"pointer",marginRight:"48px"}} fontSize="24px" color= {hover==true?"#f49101":"#3b3b3b" } />  
        </div>
       
        <Trash style={{cursor:"pointer",marginRight:"30px"}} onClick={()=>{handleRemove(item.id)}}  fontSize="24px" color="#3b3b3b" />  
        </td>
    <td>${item.price * item.quantity}.00 </td>

  </tr>
    ))}
    </tbody>
  </CustomTable>
  <Total>
    <h3>Cart Total <span>${total}.00</span></h3>
  </Total>

</>
)

}

const mapStateToProps = (state)=>{
  return{
    items: state.addedItems,
      total: state.total
  }
}
const mapDispatchToProps = (dispatch) => {
  return{
      removeItem: (id) => {dispatch(removeItem(id))},
      addQuantity: (id) => {dispatch(addQuantity(id))},
      subtractQuantity: (id) => {dispatch(subtractQuantity(id))}
      
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(CheckCart)



