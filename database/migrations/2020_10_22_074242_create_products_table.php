<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('products')) { //kiem tra xem bang products da ton tai hay chua?
            Schema::create('products', function (Blueprint $table) {
                $table->increments('id'); // cột id có kiểu là interger và tự động tăng
                $table->tinyInteger('status')->nullable();
                $table->string('title')->nullable();
                $table->text('description')->nullable();
                $table->float('price')->nullable();
                $table->float('discount')->nullable();
                $table->float('newprice')->nullable();
                $table->integer('amount')->nullable();
                $table->string('image1')->nullable();
                $table->string('image2')->nullable();
                $table->string('image3')->nullable();
                $table->string('image4')->nullable();
                $table->string('image5')->nullable();
                $table->tinyInteger('category_id')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}