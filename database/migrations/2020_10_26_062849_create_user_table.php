<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname')->nullable();
            $table->string('username')->nullable($value = false);
            $table->string('password')->nullable($value = false);
            $table->string('avatar')->nullable();
            $table->string('email')->nullable($value=false);
            $table->string('job')->nullable();
            $table->date('birthday')->nullable();
            $table->tinyInteger('age')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('fb')->nullable();
            $table->string('skype')->nullable();
            $table->string('twitter')->nullable();
            $table->string('insta')->nullable();
            $table->string('favoutites')->nullable();
            $table->integer('zipcode')->nullable();
            $table->tinyInteger('status');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
