<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class orderDetail extends Model
{
    protected $table = "order_details";

    public function product(){
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

    public function order(){
        return $this->belongsTo('App\Models\order', 'order_id', 'id');
    }
}
