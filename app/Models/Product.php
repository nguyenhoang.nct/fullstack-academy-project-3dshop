<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function category(){
        return $this->belongsTo('App\Models\categories', 'category_id', 'id');
    }

    public function order_detail(){
        return $this->hasMany('App\Models\orderDetail','product_id', 'id');
    }

    protected $fillable = [
        'title',
        'description',
        'price',
        'amount',
    ];

}