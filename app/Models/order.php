<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table = "orders";

    public function order_detail(){
        return $this->hasMany('App\Models\orderDetail', 'order_id', 'id');
    }

    public function order(){
        return $this->belongsTo('App\Models\customer', 'customer_id', 'id');
    }
}
